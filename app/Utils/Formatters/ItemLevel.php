<?php declare(strict_types=1);

namespace App\Utils\Formatters;

class ItemLevel
{
    public static function color(mixed $level): string
    {
        if ($level === '' || $level === null) {
            return '--';
        } 
        if ($level >= config('rk.ilvls.mythic')) {
            return '<span class="rk-cls-rogue">'.$level.'</span>';
        } elseif ($level >= config('rk.ilvls.heroic')) {
            return '<span class="rk-cls-demonhunter">'.$level.'</span>';
        } elseif($level >= config('rk.ilvls.normal')) {
            return '<span class="rk-cls-shaman">'.$level.'</span>';
        } elseif($level >= config('rk.ilvls.raid-finder')) {
            return '<span class="rk-cls-hunter">'.$level.'</span>';
        } else {
            return '<span style="color: #999;">'.$level.'</span>';
        }
    }
}