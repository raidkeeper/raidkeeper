<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeystonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keystones', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('character_id');
            $table->string('dungeon');
            $table->integer('level');
            $table->integer('period');
            $table->string('raiderio_url')->nullable()->default(null);
            $table->string('completed_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keystones');
    }
}
