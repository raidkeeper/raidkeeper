<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;
use Illuminate\Bus\Batch;
use App\Models\Character;

class UpdateCharacterKeystones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rk:update:character:keystones {id? : ID of the Character to be updated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule updater jobs for pending characters.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->argument('id') != null) {
            $characters = Character::find($this->argument('id'));
            if ($characters == null) {
                $this->error('You provided an invalid Character ID.');
                exit;
            }
        } else {
            $characters = Character::where('allow_fetch', true)->get();
        }

        $this->line('Scheduling pending character updates.');
        $characters = Character::where('allow_fetch', true)->get();
        foreach($characters as $character) {
            Bus::batch([
                [
                    new \App\Jobs\Character\RaiderIo\UpdateKeystones($character),
                ]
            ])->then(function (Batch $batch) use ($character) {
                Bus::chain([
                    new \App\Jobs\Character\UpdateGreatVault($character),
                ])->dispatch();
            })->name('Update Character Keystones')->dispatch();
        }
        $this->info('Enqueued ' . $characters->count() . ' Characters for keystones update.');
    }
}
