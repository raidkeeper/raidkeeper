<?php

return [
    /*
     |-------------------------------------------------------------------------
     | Core Application Language Definitions
     |-------------------------------------------------------------------------
     |
     | The following language lines are used for core application definitions
     |
     */

    'title'          => 'RaidKeeper',
    'logo'           => 'Logo',
    'news'           => 'Site News',
    'privacy_policy' => 'Privacy Policy',
    'report_bug'     => 'Report A Bug',

    // Auth
    'register'     => 'Register',
    'login'        => 'Login',

    // External Services
    'join_our' => 'Join Our',
    'discord'  => 'Discord',
    'email'    => 'Email',

    // Navigation
    'find_groups' => 'Find a Group',
];
