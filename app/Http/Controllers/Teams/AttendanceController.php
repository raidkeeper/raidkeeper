<?php

namespace App\Http\Controllers\Teams;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Character;
use App\Models\Team;
use App\Models\CharacterAttendance;
use App\Models\TeamAttendance;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function index(Team $team): \Illuminate\View\View
    {
        $user       = Auth::user();
        $characters = $team->memberCharacters();
        $periods    = TeamAttendance::orderBy('date', 'DESC')->limit(10)->get()->reverse();
        return view(config('rk.version.ui') . '.teams.attendance.index', compact('team', 'characters', 'user', 'periods'));
    }

    public function create(Team $team): \Illuminate\View\View
    {
        $user       = Auth::user();
        $characters = $team->memberCharacters();
        return view(config('rk.version.ui') . '.teams.attendance.new', compact('team', 'characters', 'user'));
    }

    public function save(Team $team, Request $request)
    {
        $teamAttendance = new TeamAttendance();
        $teamAttendance->team_id = $team->id;
        $teamAttendance->date    = $request->input('date');
        $teamAttendance->save();

        $submittedAttendance = $request->input('attendance');
        foreach ($submittedAttendance as $character_id => $status) {
            $character = Character::where('id', $character_id)->first();
            if ($character != null) {
                $attendance = new CharacterAttendance();
                $attendance->character_id       = $character->id;
                $attendance->team_id            = $team->id;
                $attendance->status             = $status;
                $attendance->team_attendance_id = $teamAttendance->id;
                $attendance->save();
            }
        }
        return redirect('/teams/'.$team->region.'/'.$team->name.'/attendance');
    }
}
