@extends('layouts.main')

@section('content')
    <div class="row mx-2">
        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-1 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-left">Account Settings</h3>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">We ain't got no settings yet.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-2 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Discord Account</h3>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Status: <span class="float-end text-success">Valid</span></li>
                            <li class="list-group-item">Username: <span class="float-end">{{$user->username}}{{$user->discrimator}}</span></li>
                            <li class="list-group-item">Visible to your Raid Leaders? <span class="float-end">Yes</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Battle.Net Card -->
            <div class="row mx-2 mt-2 mb-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Battle.Net Account</h3>
                        </div>
                        <ul class="list-group list-group-flush">
                            @if($user->battlenet()->count() == 1)
                                <li class="list-group-item">Status: <span class="float-end text-success">Connected</span></li>
                                <li class="list-group-item">Last Sync: <span class="float-end">{{$user->battlenet()->first()->updated_at->diffForHumans()}}</span></li>
                                <li class="list-group-item">
                                    <a href="/login/battlenet" class="btn btn-success float-end">
                                        <i class="fab fa-battle-net"></i> Sync Now
                                    </a>
                                </li>
                            @else
                                <li class="list-group-item">Status: <span class="float-end text-danger">Not Connected</span></li>
                                <li class="list-group-item">Last Sync: <span class="float-end">Never</span></li>
                                <li class="list-group-item">
                                    <a href="/login/battlenet" class="btn btn-primary float-end">
                                        <i class="fab fa-battle-net"></i> Connect To Battle.net
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-2">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Your Characters</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm text-center">
                        <thead>
                            <tr>
                                <th style="width: 25%">Character</th>
                                <th style="width: 15%">Specialization</th>
                                <th style="width: 10%">iLvl</th>
                                <th style="width: 10%">Level</th>
                                <th style="width: 15%">Realm</th>
                                <th>External Links</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($user->battlenet()->first() !== null)
                                @foreach($user->battlenet()->first()->characters()->get() as $character)
                                    <tr>
                                        <td class="cls-{{$character->shortClassName()}}">{{ $character->getAttr('name') }}</td>
                                        <td>
                                            <img style="height: 32px;" src="/img/icons/specs/{{ $character->iconSpec() }}.png"></img>
                                            <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png"></img>
                                        </td>
                                        <td>
                                            @if($character->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                <span class="text-warning">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                <span class="cls-demonhunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                <span class="cls-shaman">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                <span class="cls-hunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @else
                                                <span style="color: #999;">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $character->getAttr('level') }}</td>
                                        <td class="fac-{{$character->shortFaction()}}">{{ $character->getAttr('realm') }}</td>
                                        <td>
                                            <a class="btn btn-empty" href="https://www.warcraftlogs.com/character/us/{{strtolower($character->getAttr('realm'))}}/{{$character->getAttr('name')}}" target="blank">Warcraft Logs</a>
                                            <a class="btn btn-empty" href="https://raider.io/characters/us/{{strtolower($character->getAttr('realm'))}}/{{$character->getAttr('name')}}" target="blank">Raider.io</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
