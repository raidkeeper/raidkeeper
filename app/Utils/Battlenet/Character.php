<?php

namespace App\Utils\Battlenet;

use App\Models\Character as CharacterModel;
use App\Models\CharacterEquipment as Equipment;

class Character
{
    // User provided fields
    public string $region;
    public string $name;
    public string $realm;
    public int    $id;

    public function __construct($name, $realm, $region)
    {
        $this->name   = $name;
        $this->realm  = $realm;
        $this->region = $region;

        $client   = new Client();
        $response = $client->getFromEndpoint(
            $region,
            'profile',
            'wow/character/'.$this->slugify($realm).'/'.urlencode(strtolower($name)),
        );
        $data   = $response['response'];

        $lookup = CharacterModel::where(['id' => $data->id, 'region' => $region]);
        if ($lookup->count() == 1) {
            $character = $lookup->first();
        } else {
            $character         =  new CharacterModel();
            $character->id     = $data->id;
            $character->region = $region;
        }
        
        $character->name  = $data->name;
        $character->realm = $this->getLocaleData($data->realm);
        $character->save();

        $this->id = $data->id;

        $character->setAttr('gender', $this->getLocaleData($data->gender));
        $character->setAttr('faction', $this->getLocaleData($data->faction));
        $character->setAttr('race', $this->getLocaleData($data->race));
        $character->setAttr('class', $this->getLocaleData($data->character_class));
        $character->setAttr('spec', $this->getLocaleData($data->active_spec));
        $character->setAttr('level', $data->level);
        $character->setAttr('experience', $data->experience);
        $character->setAttr('equipped_ilvl', $data->equipped_item_level);
        $character->setAttr('bag_ilvl', $data->average_item_level);
        
        if (isset($data->guild) && isset($data->guild->name)) {
            $character->setAttr('guild', $data->guild->name);
        }

        if (isset($data->covenant_progress) && isset($data->covenant_progress->chosen_covenant)) {
            $character->setAttr('covenant', $this->getLocaleData($data->covenant_progress->chosen_covenant));
            $character->setAttr('renown', $data->covenant_progress->renown_level);
        }

        // Equipment
        $response = $client->getFromUrl($region, $data->equipment->href);
        if (isset($response['response']->equipped_items)) {
            foreach ($response['response']->equipped_items as $item) {
                $borrowedSpecial = false;
                $hasSocket       = false;

                if (isset($item->sockets)) {
                    foreach ($item->sockets as $socket) {
                        if (isset($socket->socket_type) && isset($socket->socket_type->type) && $socket->socket_type->type == "DOMINATION") {
                            $borrowedSpecial = true;
                        } else {
                            $hasSocket = true;
                        }
                    }
                }

                $model = Equipment::updateOrCreate(
                    ['character_id' => $character->id, 'slot' => $item->slot->type],
                    [
                        'item_id'      => $item->item->id,
                        'name'         => $this->getLocaleData($item),
                        'level'        => $item->level->value,
                        'socket'       => $hasSocket,
                        'borrowed_special' => $borrowedSpecial,
                        'enchantment'  => isset($item->enchantments) ? true : false,
                    ],
                );
                $model->save();
            }
        }

        // Keystone rating from Blizzard
        $response = $client->getFromUrl($region, $data->mythic_keystone_profile->href);
        if (isset($response['response']->current_mythic_rating->rating)) {
            $character->setAttr('bliz_mythic_rating', $response['response']->current_mythic_rating->rating);
        }
    }

    protected function slugify($name): string
    {
        $name = str_replace(' ', '-', $name);
        $name = str_replace("'", '', $name);
        return strtolower($name);
    }

    public static function slug($name): string
    {
        $name = str_replace(' ', '-', $name);
        $name = str_replace("'", '', $name);
        return strtolower($name);
    }

    protected function getLocaleData(object $array, string $locale = 'en_US')
    {
        if (isset($array->name)) {
            if (isset($array->name->$locale)) {
                return $array->name->$locale;
            }
        }
        return null;
    }
}
