<nav class="col-md-3 col-lg-2 d-md-block sidebar collapse px-0">
  <div class="d-flex flex-column flex-shrink-0 p-3 bg-dark rk-sidebar" style="width: 280px;">
    <ul class="nav nav-pills flex-column mb-auto">
      @if(Request::routeIs('team.*') && Request::route('team'))
        @include('v3._components.sidebar.team')
      @else
        @include('v3._components.sidebar.generic')
      @endif
      @if(\Auth::check())
        @include('v3._components.sidebar.user')
      @endif
    </ul>
  </div>
</nav>
