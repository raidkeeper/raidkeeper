<footer class="text-center">
  © 2020-{{date('Y')}} Raidkeeper - <a href="/contact">Contact Us</a> - <a href="/discord">Discord</a> - <a href="/feedback">Feedback</a>
</footer>