<?php

namespace App\Jobs\Team;

use App\Jobs\Unique;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;
use App\Utils\Discord\Webhook;

class ReportTeamGVStatus extends Unique
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(): void {}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $period = Cache::get('raiderio_period');
        $characters = $this->model
            ->characters()
            ->wherePivot('benched', false)
            ->withSum(
                ['great_vault as key_1' => function (Builder $query) use ($period) { $query->where('period', $period); }],
                'key_1',
            )->withExists([
                'great_vault as gv_good' => function (Builder $query) use ($period) { $query->where('period', $period)->where('key_1', '>=', 10); },
                'great_vault as gv_bad' => function (Builder $query) use ($period) { $query->where('period', $period)->where('key_1', '>=', 2); },
                'great_vault as gv_ugly' => function (Builder $query) use ($period) { $query->where('period', $period)->where('key_1', null); },
            ])->get();

        $array = [
            'good' => [],
            'bad'  => [],
            'ugly' => [],
        ];
        foreach ($characters as $character) {
            $modifier = '+';
            if ($character->gv_good) {
                $key = 'good';
            } else if ($character->gv_bad) {
                $key = 'bad';
            } else {
                $modifier = '';
                $key = 'ugly';
            }
            $array[$key][] = $modifier.$character->key_1.' '.$character->name;
        }

        $payload = [
            'username'   => 'Bootlicker Boatick',
            'avatar_url' => 'https://i.imgur.com/qYWo04F.png',
            'embeds'     => [
                [
                    'author' => [
                        'name' => 'Raidkeeper',
                    ],
                    'title'  => 'Team Great Vault Update',
                    'color'  => 15418782,
                    'fields' => [
                        [
                            'name' => 'Keystone completer grindset.',
                            'value' => count($array['good']) > 0 ? implode(PHP_EOL, $array['good']) : 'No keys +10 or better completed.',
                        ],
                        [
                            'name' => 'Baby needs a better key.',
                            'value' => count($array['bad']) > 0 ? implode(PHP_EOL, $array['bad']) : 'No below 10 keys as highest for GV.',
                        ],
                        [
                            'name' => 'No key, only sadness.',
                            'value' => count($array['ugly']) > 0 ? implode(PHP_EOL, $array['ugly']) : 'All roster characters have completed keys!',
                        ],
                    ],
                ],
            ],
        ];
        Webhook::send($payload, $this->model->getAttr('discord_webhook'), true);
    }

    /**
     * The unique model ID of the job
     */
    public function uniqueId()
    {
        return 1;
    }
}
