#!/bin/bash

## Looking for unused functions
for FILE in app/Models/*.php; do
    MODEL=$(echo "$FILE" | cut -d. -f1 | cut -d/ -f3)

    for FUNC in $(grep "function" $FILE | cut -d\( -f1 | awk '{print $3}'); do
        APP_COUNT=$(grep -R "$FUNC(" app/ |wc -l)
        VIEWS_COUNT=$(grep -R "$FUNC(" resources/views/v2/ | wc -l)
        if [[ "$((APP_COUNT + VIEWS_COUNT))" -le 1 ]]; then
            echo "Function $FUNC from model $MODEL appears to be unused."
        fi
    done
done