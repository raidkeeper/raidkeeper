<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Team;

class MetricsController extends Controller
{
    public function keystones(Team $team, Request $request): \Illuminate\View\View
    {
        if ($request->input('minimum_keystone_level') == null) {
            $level = $team->attributes()->where('key', 'keystone_target_level')->first()->value ?? 15;
        } else {
            $level = $request->input('minimum_keystone_level');
        }

        $currentPeriod = Cache::get('raiderio_period');

        $lookup     = $team->characters();
        $characters = $lookup->with([
            'keystones'  => function (HasMany $query) use ($level, $currentPeriod) { $query->where('level', ">=", $level)->where('period', '>=', $currentPeriod - 5); },
        ])->orderBy('pivot_role')->get();

        return view(config('rk.version.ui') . '.teams.metrics.keystones', compact('team', 'characters', 'currentPeriod', 'level'));
    }
}
