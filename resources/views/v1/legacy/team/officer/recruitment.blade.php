@extends('layouts.team-officer')

@section('subcontent')
    <div class="row mx-2">
        <!-- Current Needs -->
        <div class="col-md-6 col-sm-12 subsection">
            <div class="row mx-0 mt-4 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Recruitment Needs</h3>
                        </div>
                        <div class="card-body">
                            <div class="row px-5 py-2" style="border-bottom: 1px solid #333">
                                <div class="col-3">
                                    Tanks:
                                </div>
                                <div class="col-9">
                                    Not yet implemented
                                </div>
                            </div>
                            <div class="row px-5 py-2" style="border-bottom: 1px solid #333">
                                <div class="col-3">
                                    Healers:
                                </div>
                                <div class="col-9">
                                    Not yet implemented
                                </div>
                            </div>
                            <div class="row px-5 py-2">
                                <div class="col-3">
                                    DPS:
                                </div>
                                <div class="col-9">
                                    Not yet implemented
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Recruitment Status -->
        <div class="col-md-6 col-sm-12 subsection">
            <div class="row mx-0 mt-4 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Recruitment Status</h3>
                        </div>
                        <div class="card-body text-center text-{{$team->getAttr('recruitment') == 'open' ? 'success' : 'danger'}}">
                            <i style="width: 128px; height: 128px;" class="fas fa-{{$team->getAttr('recruitment') == 'open' ? 'check' : 'times'}}-circle"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Applications -->
        <div class="col-12 subsection">
            <div class="row mx-0 mt-4 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Pending Applications</h3>
                        </div>
                        <div class="card-body">
                            @if ($applications->count() == 0)
                                <h4 class="text-success text-center">
                                    There are no pending applications.
                                </h4>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-sm-text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Character</th>
                                                <th>Discord Account</th>
                                                <th style="width: 10%">Information</th>
                                                <th style="width: 10%">iLvl</th>
                                                <th>Links</th>
                                                <th style="width: 10%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($applications->get() as $app)
                                                <tr class="text-center">
                                                    <td class="cls-{{$app->character()->shortClassName()}}">{{ $app->character()->getAttr('name') }}</td>
                                                    <td>{{$app->character()->getUsername()}}</td>
                                                    <td>
                                                        <img style="height: 32px;" src="/img/icons/specs/{{ $app->character()->iconSpec() }}.png"></img>
                                                        <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $app->character()->iconCovenant() }}.png"></img>
                                                    </td>
                                                    <td>
                                                        @if($team->getAttr('minimum_ilvl') && $app->character()->getAttr('ilvl_equipped') < $team->getAttr('minimum_ilvl'))
                                                            <span class="cls-deathknight">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                        @else
                                                            @if($app->character()->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                                <span class="text-warning">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                            @elseif($app->character()->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                                <span class="cls-demonhunter">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                            @elseif($app->character()->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                                <span class="cls-shaman">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                            @elseif($app->character()->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                                <span class="cls-hunter">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                            @else
                                                                <span style="color: #999;">{{ $app->character()->getAttr('ilvl_equipped') }}</span>
                                                            @endif
                                                        @endif
                                                    <td>
                                                        <a class="btn btn-empty" href="https://www.warcraftlogs.com/character/us/{{strtolower($app->character()->getAttr('realm'))}}/{{$app->character()->getAttr('name')}}" target="blank">WCL</a>
                                                        <a class="btn btn-empty" href="https://raider.io/characters/us/{{strtolower($app->character()->getAttr('realm'))}}/{{$app->character()->getAttr('name')}}" target="blank">R.io</a>
                                                    </td>
                                                    <td>
                                                        <a href="/teams/{{$team->id}}/recruitment/{{$app->id}}/accept"><i class="text-success fas fa-plus"></i></a>
                                                        <a href="/teams/{{$team->id}}/recruitment/{{$app->id}}/reject"><i class="text-danger fas fa-times"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
