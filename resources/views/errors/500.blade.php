@extends('errors.base')

@section('code', __('Internal Server Error: 500'))
@section('message', __('Something broke. It may have been us, or it may have been Blizzard. Submit a bug report using the feedback button at the bottom of the page..'))