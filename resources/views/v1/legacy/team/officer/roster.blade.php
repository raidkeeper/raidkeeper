@extends('layouts.team-officer')

@section('subcontent')
<div class="row mx-2">
    <!-- Active Roster -->
    <div class="col-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Active Roster</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="active-roster" class="table table-sm text-center">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">Role</th>
                                        <th style="width: 20%">Character</th>
                                        <th style="width: auto">Account</th>
                                        <th style="width: 10%">Details</th>
                                        <th style="width: 5%">iLvl</th>
                                        <th style="width: 20%">Raidwides</th>
                                            <th style="width: 8%">Actions</th>
                                            <th style="width: 15%;">External Links</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($team->roster()->get() as $spot)
                                        @if($spot->character() != null)
                                            <tr>
                                                <td>
                                                    <a href="/teams/{{$team->id}}/roster/{{$spot->id}}/role">
                                                        <img style="width: 32px; height: 32px;" src="/img/icons/{{ $spot->roleToText() }}.svg"></img>
                                                    </a>
                                                </td>
                                                <td class="cls-{{$spot->character()->shortClassName()}}">
                                                    {{ $spot->character()->getAttr('name') }}
                                                </td>
                                                <td>{{$spot->character()->getUsername()}}</td>
                                                <td>
                                                    <img style="height: 32px;" src="/img/icons/specs/{{ $spot->character()->iconSpec() }}.png"></img>
                                                    <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $spot->character()->iconCovenant() }}.png"></img>
                                                </td>
                                                <td>
                                                    @if($team->getAttr('minimum_ilvl') && $spot->character()->getAttr('ilvl_equipped') < $team->getAttr('minimum_ilvl'))
                                                        <span class="cls-deathknight">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                    @else
                                                        @if($spot->character()->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                            <span class="text-warning">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                        @elseif($spot->character()->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                            <span class="cls-demonhunter">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                        @elseif($spot->character()->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                            <span class="cls-shaman">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                        @elseif($spot->character()->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                            <span class="cls-hunter">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                        @else
                                                            <span style="color: #999;">{{ $spot->character()->getAttr('ilvl_equipped') }}</span>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($spot->character()->hasRaidBuff())
                                                        <img style="height: 32px; width: 32px;" src="/img/icons/spells/{{$spot->character()->getRaidBuff()}}.jpg"></img>
                                                    @endif
                                                    @foreach($spot->character()->getRaidCooldown() as $cooldown)

                                                        <img style="height: 32px; width: 32px;" src="/img/icons/spells/{{$cooldown}}.jpg"></img>
                                                    @endforeach
                                                </td>

                                                <td>
                                                    <form action="/teams/{{$team->id}}/roster" method="POST">
                                                        @csrf
                                                        {{ method_field('DELETE') }}
                                                        <input type="hidden" name="character_id" value="{{$spot->character()->id}}">
                                                        <input class="btn btn-empty" style="border-color: red; color: red;" type="submit" name="submit" value="Bench">
                                                    </form>
                                                </td>

                                                <td>
                                                    <a class="btn btn-empty" href="https://www.warcraftlogs.com/character/us/{{strtolower($spot->character()->getAttr('realm'))}}/{{$spot->character()->getAttr('name')}}" target="blank">WCL</a>
                                                    <a class="btn btn-empty" href="https://raider.io/characters/us/{{strtolower($spot->character()->getAttr('realm'))}}/{{$spot->character()->getAttr('name')}}" target="blank">R.io</a>
                                                </td>

                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('active-roster').DataTable()
                            } );
                        </script>-->
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Available Alts -->
    <div class="col-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Available Characters</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="active-roster" class="table table-sm text-center">
                                <thead>
                                    <tr>
                                        <th style="width: 20%">Character</th>
                                        <th style="width: auto">Account</th>
                                        <th style="width: 10%">Details</th>
                                        <th style="width: 5%">iLvl</th>
                                        <th style="width: 20%">Raidwides</th>
                                        <th style="width: 8%">Actions</th>
                                        <th style="width: 15%;">External Links</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($members as $user)
                                        @if($user->battlenet()->first() != null)
                                            @foreach($user->battlenet()->first()->characters()->get() as $character)
                                                @if ($team->roster()->where('character_id', $character->id)->count() == 0)
                                                    @if(strtolower($team->getAttr('faction')) == strtolower($character->getAttr('faction')))
                                                        <tr>
                                                            <td class="cls-{{$character->shortClassName()}}">
                                                                {{ $character->getAttr('name') }}
                                                            </td>
                                                            <td>{{$character->getUsername()}}</td>
                                                            <td>
                                                                <img style="height: 32px;" src="/img/icons/specs/{{ $character->iconSpec() }}.png"></img>
                                                                <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png"></img>
                                                            </td>
                                                            <td>
                                                                @if($team->getAttr('minimum_ilvl') && $character->getAttr('ilvl_equipped') < $team->getAttr('minimum_ilvl'))
                                                                    <span class="cls-deathknight">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                @else
                                                                    @if($character->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                                        <span class="text-warning">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                                        <span class="cls-demonhunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                                        <span class="cls-shaman">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                                        <span class="cls-hunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                    @else
                                                                        <span style="color: #999;">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($character->hasRaidBuff())
                                                                    <img style="height: 32px; width: 32px;" src="/img/icons/spells/{{$character->getRaidBuff()}}.jpg"></img>
                                                                @endif
                                                                @foreach($character->getRaidCooldown() as $cooldown)

                                                                    <img style="height: 32px; width: 32px;" src="/img/icons/spells/{{$cooldown}}.jpg"></img>
                                                                @endforeach
                                                            </td>

                                                            <td>
                                                                <form action="/teams/{{$team->id}}/roster" method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="character_id" value="{{$character->id}}">
                                                                    <input type="hidden" name="user_id" value="{{$character->battlenet()->first()->user()->first()->id}}">
                                                                    <input class="btn btn-empty" style="border-color: green; color: green;" type="submit" name="submit" value="Activate">
                                                                </form>
                                                            </td>

                                                            <td>
                                                                <a class="btn btn-empty" href="https://www.warcraftlogs.com/character/us/{{strtolower($character->getAttr('realm'))}}/{{$character->getAttr('name')}}" target="blank">WCL</a>
                                                                <a class="btn btn-empty" href="https://raider.io/characters/us/{{strtolower($character->getAttr('realm'))}}/{{$character->getAttr('name')}}" target="blank">R.io</a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('active-roster').DataTable()
                            } );
                        </script>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
