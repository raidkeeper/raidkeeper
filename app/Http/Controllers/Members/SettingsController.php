<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class SettingsController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        return view('v2.members.settings', compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $whitelisted = ['theme-color'];

        foreach($whitelisted as $key) {
            if ($request->input($key) != null) {
                $user->setSetting($key, $request->input($key));
            }
        }
        return redirect()->back();
    }
}
