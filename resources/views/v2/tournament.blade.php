@extends('v2.core.main')

@section('headline')
{{$team->name}}
@endsection

@section('content')
<section class="mb-3 mb-lg-5">
    <div class="row">
        <!-- Participants -->
        <div class="col-xl-3 col-12 mb-4">
            <div class="card-widget h-100" style="display: block">
                <div class="text-center">
                    <div class="text text-center">
                        <h1 class="mb-0">{{ count($participants) }}</h1>
                        <span class="text-gray-300">participants</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Participants -->
        <div class="col-xl-4 col-12 mb-4">
            <div class="card-widget h-100" style="display: block">
                <div class="text-center">
                    <div class="text text-center">
                        <div>
                        @foreach((\Illuminate\Support\Facades\Cache::get('raiderio_affixes') ?? []) as $affix)
                            <img style="padding-left: 3px; padding-right: 3px;" src="https://wow.zamimg.com/images/wow/icons/large/{{$affix->icon}}.jpg" alt="{{$affix->name}} Icon">
                        @endforeach
                        </div>
                        <span class="text-gray-300">weekly affixes</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Num Keystones Completed -->
        <div class="col-xl-3 col-12 mb-4">
            <div class="card-widget h-100" style="display: block">
                <div class="text-center">
                    <div class="text text-center">
                        <h1 class="mb-0">{{ count($keystones) }}</h1>
                        <span class="text-gray-300">completed dungeons</span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Keystones Section -->
<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms" style="margin-left: 0; font-size: 2rem;">Dungeon Run Log</h2 >
    <div class="row">
        <div class="col px-2 mb-4 mb-lg-0">
            <table class="table table-hover mb-0" id="rosterTable">
                <thead>
                    <th>Dungeon</th>
                    <th style="text-align: center;">Key Level</th>
                    <th style="text-align: center;">Score</th>
                    <th style="text-align: center;">Completed</th>
                    <th style="text-align: center;">Time Difference</th>
                    <th style="text-align: center;">% Over/Under</th>
                    <th>Link</th>
                </thead>
                <tbody>
                    @foreach($keystones as $key)
                    <tr >
                        <td>{{$key->dungeon}}</td>
                        <td style="text-align: center;">{{$key->level}}</td>
                        <td style="text-align: center;">
                            @foreach($scoreColors as $score => $hex)
                                @if ($key->score >= ($score / 16))
                                    <span style="color: {{$hex}}"> {{$key->score}}</span>
                                    @break;
                                @endif
                            @endforeach
                        </td>
                        <td style="text-align: center;">{{date_format(date_create($key->completed_at), "M d, H:i")}}</td>
                        @php
                            $clearTimer = $key->clear_time_ms / 1000;
                            $remaining  = floor($instanceTimers[$key->dungeon] - $clearTimer);
                            $isOver     = $remaining <= 0;

                            $minutes = floor($remaining / 60);
                            $seconds = floor($remaining - ($minutes * 60));

                            $minutes = $minutes < 10 ? sprintf("0%d", $minutes) : $minutes;
                            $seconds = $seconds < 10 ? sprintf("0%d", $seconds) : $seconds;

                            $percentage = $remaining / $instanceTimers[$key->dungeon];

                            $text = sprintf("%s:%s", $minutes, $seconds);
                        @endphp
                        <td style="text-align: center;">
                            @if ($isOver)
                                <span class="rk-cls-deathknight">-{{$text}}</span>
                            @else
                                <span class="rk-cls-monk">{{$text}}</span>
                            @endif
                        </td>
                        <td style="text-align:center;">{{round($percentage * 100, 2)}}%</td>
                        <td>
                            <a target="blank" href="{{$key->raiderio_url}}">
                                <img style="height: 32px; width: 32px;" src="/static/logos/raiderio.png" alt="Raider IO"> 
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                    
        </div>
    </div>
</section>
<!-- End Keystones Section -->

<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms" style="margin-left: 0; font-size: 2rem;">Participants</h2 >
    <div class="row">
        <div class="col px-2 mb-4 mb-lg-0">
            <table class="table table-hover mb-0" id="rosterTable">
                <thead>
                    <th>Name</th>
                    <th style="text-align: center;">Raider.io Score</th>
                    <th style="text-align: center;">Item Level</th>
                    <th style="text-align: center;">Completed Keys</th>
                    <th style="text-align: center;">Highest Key</th>
                    <th style="text-align: center;">Links</th>
                </thead>
                <tbody>
                    @foreach($characters as $character)
                    <tr>      
                        <td class="rk-cls-{{str_replace(' ', '', strtolower($character->attributes->where('key', 'class')->first()->value))}}">
                            <img style="height: 32px; width: 32px;" src="/static/icons/classes/{{str_replace(' ', '', strtolower($character->attributes->where('key', 'class')->first()->value ?? 'Unknown'))}}.png">
                            {{$character->name}}
                        </td>
                        <td style="text-align: center;">
                            @foreach($scoreColors as $score => $hex)
                                @if (($character->attributes->where('key', 'bliz_mythic_rating')->first()->value ?? 0) >= $score)
                                    <span style="color: {{$hex}}">{{$character->attributes->where('key', 'bliz_mythic_rating')->first()->value ?? '0'}}</span>
                                    @break;
                                @endif
                            @endforeach
                        </td>
                        <td style="text-align: center;">
                            {{$character->attributes->where('key', 'equipped_ilvl')->first()->value}}
                        </td>
                        @if($participantKeystones[$character->id]['count'] > 0)
                            <td style="text-align: center;">
                                {{$participantKeystones[$character->id]['count']}}
                            </td>
                            <td style="text-align: center;">
                                +{{$participantKeystones[$character->id]['highest']}}
                            </td>
                        @else
                            <td style="text-align: center;">
                                <span style="color: #999;">---</span>
                            </td>
                            <td style="text-align: center;">
                                <span style="color: #999;">---</span>
                            </td>
                        @endif
                        <td style="text-align: center;">
                            <a href="https://worldofwarcraft.com/en-us/character/us/{{$character->realm}}/{{$character->name}}">
                                <img style="width: 32px; height: 32px;" src="/static/logos/wow.png" alt="Blizzard Armory">
                            </a> 

                            <a href="https://raider.io/characters/us/{{$character->realm}}/{{$character->name}}">
                                <img style="width: 32px; height: 32px;" src="/static/logos/raiderio.png" alt="Raider IO">
                            </a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                    
        </div>
    </div>
</section>
@endsection