<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TeamAttribute;
use App\Models\TeamOfficer;
use App\Models\User;

class SettingsController extends Controller
{
    public function index(Team $team): \Illuminate\View\View
    {
        $owner    = User::find($team->user_id)->battletag ?? '---';
        $settings = $team->attributes()->get();

        //dd($settings->where('key', 'public')->first()->value);

        $public = filter_var($settings->where('key', 'public')->first()->value ?? false, FILTER_VALIDATE_BOOLEAN);

        // Default roster columns
        $teamSettings = [
            'default_roster_columns' => [
                'name'          => 'Default Roster Columns',
                'description'   => "These are the columns that will show by default for your team's roster view.",
                'attribute_key' => 'default_roster_columns',
                'field'         => 'columns[]',
                'options'     => [
                    'great_vault'  => '',
                    'mythic_score' => '',
                    'tier_special' => '',
                ]
            ],
            'hidden_raidbuffs' => [
                'name'        => 'Hidden Raidwide Buffs',
                'description' => 'These are the raidwide buffs you want to hide on the roster.',
                'attribute_key' => 'hidden_raidbuffs',
                'field'         => 'raidbuffs[]',
                'options'     => [
                    'battle-shout'     => '',
                    'fortitude'        => '',
                    'arcane-intellect' => '',
                    'devotion-aura'    => '',
                ],
            ],
            'hidden_raidcds' => [
                'name'          => 'Hidden Healing Cooldowns',
                'description'   => 'These are the healing cooldowns you want to hide on the roster.',
                'attribute_key' => 'hidden_raidbuffs',
                'field'         => 'raidbuffs[]',
                'options'     => [
                    'aura-mastery'    => '',
                    'rallying-cry'    => '',
                    'revival'         => '',
                    'tranquility'     => '',
                    'spirit-link'     => '',
                    'healing-tide'    => '',
                    'anti-magic-zone' => '',
                ],
            ],
            'hidden_raidmajors' => [
                'name'          => 'Hidden Damage Assists',
                'attribute_key' => 'hidden_raidbuffs',
                'description'   => 'These are the healing cooldowns you want to hide on the roster.',
                'field'         => 'raidbuffs[]',
                'options'       => [
                    'chaos-brand' => '',
                    'mystic-touch'=> '',
                    'windfury'    => '',
                ],
            ],
        ];
        foreach($teamSettings as $setting => $array) {
            $lookup = $settings->firstWhere('key', $array['attribute_key'])->value ?? null;
            $lookup = $lookup === null ? [] : explode(',', $lookup);
            foreach($lookup as $k => $v) {
                if(isset($teamSettings[$setting]['options'][$v])) {
                    $teamSettings[$setting]['options'][$v] = 'checked';
                }
            }
        }
        //dd($teamSettings);

        return view(config('rk.version.ui') . '.teams.settings.index', compact('team', 'owner', 'public', 'teamSettings', 'settings'));
    }

    public function create(Team $team, Request $request)
    {
        $attributes = [
            'callouts' => 'overview_callout_sections',
            'columns'  => 'default_roster_columns',
            'raidbuffs' => 'hidden_raidbuffs',
            'keystone_target_level' => 'keystone_target_level',
            'public' => 'public',
        ];
        foreach ($attributes as $attribute => $key) {
            if ($request->input($attribute) !== null) {
                $input = implode(',', $request->input($attribute));
                $model = $team->attributes()->where('key', $key)->first() ?? new TeamAttribute(['key' => $key]);
                $model->value = $input;
                $model->team_id = $team->id;
                $model->save();
            }
            
        }

        if ($request->input('team_name') != null) {
            $team->name = $request->input('team_name');
        }

        $team->save();
        $team->cacheRoster();
        return redirect($team->url().'/settings', 302);
    }

    public function promote(Team $team, Request $request)
    {
        $battletag = $request->input('battletag');
        $user = User::where('battletag', $battletag)->first();
        if ($user === null) {
            return redirect()->back()->with([
                'message' => 'Battlenet user '.$battletag.' has to log in once before they can be made an officer.',
                'alert'   => 'danger'
            ]);
        }
        if ($user->officerships()->where('team_id', $team->id)->count() == 1) {
            return redirect()->back()->with([
                'message' => 'Battlenet user '.$battletag.' is already an officer.',
                'alert'   => 'primary'
            ]);
        }
        $officership = New TeamOfficer();
        $officership->user_id = $user->id;
        $officership->team_id = $team->id;
        $officership->save();
        return redirect()->back()->with([
            'message' => 'Battlenet user '.$battletag.' added as an officer.',
            'alert'   => 'success'
        ]);
    }

    public function demote(Team $team, Request $request)
    {
        $officership = TeamOfficer::where([
            'user_id' => $request->input('user_id'),
            'team_id' => $team->id,
        ])->first();
        if ($officership != null) {
            $officership->delete();
            return redirect()->back()->with([
                'message' => 'User removed from Team officers.',
                'alert'   => 'success'
            ]);
        }
        return redirect()->back()->with([
            'message' => 'User is not an officer.',
            'alert'   => 'warning'
        ]);

    }
}
