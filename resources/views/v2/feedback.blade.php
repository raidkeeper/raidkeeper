@extends('v2.core.main')

@section('headline')
Submit A Support Request
@endsection

@section('content')
<!-- Welcome Section -->
<section class="mb-4 mb-lg-5">
    <div class="row">
        <div class="col-lg-6 mb-4 mb-lg-0">
            <h2 class="section-heading section-heading-ms mb-3 mb-lg-3">Please let us know if you have issues, suggestions, or ideas!</h2>
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <form action="/feedback" method="POST">
                        @csrf
                        <input type="hidden" name="referring_page" value="{{$referer}}">
                        <div class="input-group mb-4">
                            <label class="input-group-text" for="type">Type:</label>
                            <select class="form-select" name="type" id="type" required>
                                <option value="support" selected>Question / Issue</option>
                                <option value="bug">Bug</option>
                                <option value="iteration">Improvement</option>
                                <option value="greenfield">Feature Request</option>
                            </select>
                        </div>
                        <div class="input-group mb-4">
                            <label class="input-group-text" for="name">You Are:</label>
                            <input class="form-control type="text" name="name" id="name" value="{{ \Auth::check() ? \Auth::user()->battletag : 'Anonymous' }}" readonly>
                        </div>

                        <div class="form-text rk-accent-10 mb-1">Can we contact you about this submission?</div>
                        <div class="input-group mb-4">
                            <label class="input-group-text" for="contact">Contact Me:</label>
                            <select class="form-select" name="contact" id="contact" required>
                              <option value="yes">Yes</option>
                              <option value="no" selected>No</option>
                          </select>
                        </div>
                        <div class="form-text rk-accent-10 mb-1">If we can contact you, what is your Discord? (Optional)</div>

                        <div class="input-group mb-4">
                            <label class="input-group-text" for="discord">Discord:</label>
                            <input class="form-control" type="text" name="discord" id="discord" placeholder="This is only used if we can contact you about this request">
                        </div>
                        <div class="form-text rk-accent-10 mb-1">Please describe your request in as much detail as possible.</div>

                        <div class="input-group mb-4">
                            <label class="input-group-text" for="request">Your Request:</label>
                            <textarea class="form-control" name="request" id="request" required></textarea>
                        </div>
                        <div class="form-text rk-accent-10 mb-3">Please consider joining the <a href="https://discord.gg/GqsVdhnr" target="blank">Raidkeeper Discord</a> for quicker support.</div>

                        <div class="input-group text-right align-items-right">
                            <input type="submit" class="btn btn-primary text-right" value="Submit Request">
                        </div>
                        <div class="form-group"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection