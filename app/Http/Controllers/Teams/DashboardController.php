<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Utils\RaiderIo\MythicPlus;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller
{
    public function index(Team $team): \Illuminate\View\View
    {
        $period  = Cache::get('raiderio_period');

        // Getting team details
        $attributes = $team->attributes();
        $callouts   = $attributes->where('key', 'overview_callout_sections')->first()->value ?? "";
        $keyTarget  = $attributes->where('key', 'keystone_target')->first()->value ?? 15;

        // Getting keystone information

        $characters = $team->characters()->with([
            'attributes' => function (HasMany $query) { $query->where('key', 'equipped_ilvl'); },
        ])->withCount([
            'keystones'  => function (Builder $query) use ($period) { $query->where('period', $period); },
        ])->withExists([
            'keystones as target_keystone_count' => function (Builder $query) use ($period, $keyTarget) { $query->where('period', $period)->where('level', '>=', $keyTarget); },
        ])->get();

        $callouts = $team->attributes()->where('key', 'overview_callout_sections')->first()->value ?? "";
        $ilvl_callouts = [];
        if (str_contains($callouts, 'tank_ilvl')) {
            $ilvl_callouts['tank'] = 1;
        }
        if (str_contains($callouts, 'healer_ilvl')) {
            $ilvl_callouts['healer'] = 2;
        }

        return view(config('rk.version.ui') . '.teams.dashboard', compact('team', 'characters', 'callouts', 'ilvl_callouts', 'keyTarget'));
    }
}
