@extends('v2.core.main')

@section('headline')
<span class="rk-cls-{{$character->getClass(true)}}">{{ $character->name }}</span>
@endsection

@section('content')
<script>const whTooltips = {colorLinks: true, iconizeLinks: true, renameLinks: true, iconSize: 'medium'};</script>
<script src="https://wow.zamimg.com/js/tooltips.js"></script>
<section class="mb-3 mb-lg-5">
    <div class="row">
        <!-- Active Raider Count -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card-widget h-100">
                <div class="card-widget-body">
                    <div class="text text-center">
                        <h3 class="mb-0 rk-cls-{{$character->getClass(true)}}">{{$character->getClass()}}</h3>
                        <span class="text-gray-300">character class</span>
                    </div>
                </div>
                <div class="icon text-white rk-bg-{{$character->getClass(true)}}"><i class="fas fa-dumbbell fa-2x"></i></div>
            </div>
        </div>

        <!-- Keystones Completed Weekly -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card-widget h-100">
                <div class="card-widget-body">
                    <div class="text text-center">
                        <h3 class="mb-0 text-blue">
                            {{$character->attributes()->where('key','equipped_ilvl')->first()->value ?? "" }}
                        </h3>
                        <span class="text-gray-300">equipped item level</span>
                    </div>
                </div>
                <div class="icon text-white bg-blue"><i class="fas fa-dumbbell fa-2x"></i></div>
            </div>
        </div>

        <!-- Healer Average ilvl -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card-widget h-100">
                <div class="card-widget-body">
                    <div class="text text-center">
                        <h3 class="mb-0 text-pink">
                            @if($character->getHighestWeeklyKeystone($period) === null)
                                {{ '---'}}
                            @else
                                +{{$character->getHighestWeeklyKeystone($period)->level}}
                            @endif
                        </h3>
                        <span class="text-gray-300">highest weekly key</span>
                    </div>
                </div>
                <div class="icon text-white bg-pink"><i class="fas fa-star fa-2x"></i></div>
            </div>
        </div>

        <!-- raid average ilvl -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card-widget h-100">
                <div class="card-widget-body">
                    <div class="text text-center">
                        <h3 class="mb-0 text-yellow">{{$character->keystones()->where('period', $period)->count()}}</h3>
                        <span class="text-gray-300">completed keys this week</span>
                    </div>
                </div>
                <div class="icon text-white bg-yellow"><i class="fas fa-key fa-2x"></i></div>
            </div>
        </div>
    </div>
</section>

<!-- Keystones Section -->
<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms mb-4 mb-lg-5">Character Keystone Overview</h2>
    <div class="row">
        <div class="col-lg-7 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-header">
                    <h4 class="card-heading">Recent Completed Keys</h4>
                </div>
                <div class="card-body px-2 py-2">
                    <!-- Keystone Line -->
                    @foreach($character->keystones()->where('period', $period)->limit(3)->get() as $key)
                        <div class="col-12 keystone-feed">
                            <a class="message card px-5 py-3 mb-4 bg-hover-gradient-primary text-decoration-none text-reset" href="#">
                                <div class="row">
                                    <div class="col-xl-3 d-flex align-items-center flex-column flex-xl-row text-center text-md-left">
                                        <strong class="h5 mb-0">{{(new \Carbon\Carbon($key->completed_at))->diffForHumans(null, null, true)}}</strong>
                                        <img class="avatar avatar-md p-1 mx-3 my-2 my-xl-0" src="/static/icons/dungeons/{{str_replace(' ', '', strtolower($key->dungeon))}}.jpg" alt="dungeon icon for {{$key->dungeon}}" style="max-width: 3rem">
                                        <h4 class="mb-0">+{{ $key->level }}</h4>
                                    </div>
                                    <div class="col-xl-9 d-flex align-items-center flex-column flex-xl-row text-center text-md-left">
                                        <p class="mb-0 mt-3 mt-lg-0"> 
                                            {{ $key->dungeon }} by <strong class="h6">{{$character->name}}</strong>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Domination Socket Breakout -->
            <div class="h-100 pb-4 pb-lg-2">
                <div class="card h-100">
                    <div class="card-body d-flex">
                        <div class="row w-100 align-items-center">
                            <div class="col-12 mb-4 mb-sm-0 text-center">
                                <span class="h3 text-muted text-uppercase small">Links To External Tools</span>

                            </div>
                            <div class="col-12 d-flex flex-row justify-content-around pt-4" style="height: 130px;">
                                <div class="text-center">
                                    <a target="blank" href="https://worldofwarcraft.com/en-us/character/{{$character->region}}/{{$character->realm}}/{{$character->name}}">
                                        <img style="height: 64px; width: 64px;" src="/static/logos/wow.png" alt="Wow Armory">
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a target="blank" href="https://raider.io/characters/{{$character->region}}/{{$character->realm}}/{{$character->name}}">
                                        <img style="height: 64px; width: 64px;" src="/static/logos/raiderio.png" alt="Raider IO">
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a target="blank" href="https://www.warcraftlogs.com/character/{{$character->region}}/{{$character->realm}}/{{$character->name}}">
                                        <img style="height: 64px; width: 64px;" src="/static/logos/warcraftlogs.png" alt="WarcraftLogs">
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a target="blank" href="https://www.raidbots.com/simbot/quick?region={{$character->region}}&realm={{$character->realm}}&name={{$character->name}}">
                                        <img style="height: 64px; width: 64px;" src="/static/logos/raidbots.png" alt="Raidbots">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Keystones Section -->

<!-- Equipment Section -->
<section class="mb-4 mb-lg-5">
    <div class="card card-table mb-4">
        <div class="card-header">
            <div class="h3 text-gray-100 text-uppercase">Currently Equipped Items</div>
        </div>
        <table class="table table-hover mb-0" id="datatable2">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Level</th>
                    <th>Modifiers</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($equipment as $item)
                    @if($item != null)

                        <tr>
                            <td style="font-weight: 700;">
                                <a href="#" data-wowhead="item={{$item->item_id}}&ilvl={{$item->level}}&bonus={{$item->bonuses}}">
                                    {{$item->name}}
                                </a>
                            </td>
                            
                            <td>
                                @if($item->level >= config('rk.ilvls.mythic'))
                                <span class="rk-cls-rogue">{{ $item->level }}</span>
                                @elseif($item->level >= config('rk.ilvls.heroic'))
                                    <span class="rk-cls-demonhunter">{{ $item->level }}</span>
                                @elseif($item->level >= config('rk.ilvls.normal'))
                                    <span class="rk-cls-shaman">{{ $item->level }}</span>
                                @elseif($item->level >= config('rk.ilvls.raid-finder'))
                                    <span class="rk-cls-hunter">{{ $item->level }}</span>
                                @else
                                    <span style="color: #999;">{{ $item->level }}</span>
                                @endif
                            </td>
                            <td>
                                @if($item->borrowed_special)
                                    <span class="rk-cls-rogue">Tier Set</span>
                                @endif
                            </td>
                            <td>
                                @if($item->enchantment)
                                    <span class="rk-cls-shaman">Enchanted</span>
                                @endif
                            </td>
                            <td>
                                @if($item->socket)
                                    <span class="rk-cls-monk">Socketed</span>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection