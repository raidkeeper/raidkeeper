<footer class="footer bg-content shadow align-self-end pt-5 px-xl-5 w-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 text-center text-md-start fw-bold">
                <p class="h6 mb-2 mb-md-0">&copy; 2021 - {{date('Y')}} Raidkeeper. All rights reserved.</p>
            </div>
            <div class="col-md-6 text-center text-md-end text-gray-400">
                <p class="mb-0">
                    {{config('rk.version.application')}} | 
                    {{ round(microtime(true) - LARAVEL_START, 3) * 1000 }}ms | 
                    <a class="btn btn-sm btn-primary" href="/feedback?referer={{urlencode(url()->current())}}">Feedback</a>
                </p>
            </div>
        </div>
    </div>
</footer>
