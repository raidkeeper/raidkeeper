<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGreatVaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('great_vaults', function (Blueprint $table) {
            $table->id();
            $table->integer('character_id');
            $table->integer('period');
            $table->string('raid_1')->nullable();
            $table->string('raid_2')->nullable();
            $table->string('raid_3')->nullable();
            $table->integer('key_1')->nullable();
            $table->integer('key_2')->nullable();
            $table->integer('key_3')->nullable();
            $table->string('pvp_1')->nullable();
            $table->string('pvp_2')->nullable();
            $table->string('pvp_3')->nullable();
            $table->timestamps();

            $table->unique(['character_id', 'period']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('great_vaults');
    }
}
