<?php

namespace App\Utils\RaiderIo;

class Client
{

    public function getFromUrl(string $url): array
    {
        $headers = [
            'User-Agent: RaidKeeper <github.com/raidkeeper/raidkeeper>',
            'Accept: application/json',
        ];
        $response = $this->get($url, $headers);
        return $response;
    }

    public function get(string $url, array $headers)
    {

        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL            => $url,
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CONNECTTIMEOUT => 15,
                CURLOPT_TIMEOUT        => 60,
            ),
        );

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status == 200) {
            return [
                // @phpstan-ignore-next-line
                'response' => json_decode($response),
                'error'    => null,
            ];
        }
        return [
            'response' => $response,
            'error'    => $status,
        ];
    }
}
