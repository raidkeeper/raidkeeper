<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableKeystonesAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keystones', function (Blueprint $table) {
            $table->integer('num_keystone_upgrades')->nullable();
            $table->integer('clear_time_ms')->nullable();
            $table->float('score')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('keystones', function (Blueprint $table) {
            $table->dropColumn('num_keystone_upgrades');
            $table->dropColumn('clear_time_ms');
            $table->dropColumn('score');
        });
    }
}
