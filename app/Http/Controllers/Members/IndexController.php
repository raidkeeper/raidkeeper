<?php

namespace App\Http\Controllers\Members;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Auth;

class IndexController extends Controller
{
    public function index(): \Illuminate\View\View
    {
        $user       = Auth::user();
        $period   = Cache::get('raiderio_period');

        $characters = $user->characters()->with([
            'keystones'  => function (BelongsToMany $query) use ($period) { $query->where('period', $period); },
            'great_vault' => function (HasMany $query) use ($period) { $query->where('period', $period); },
            'attributes' => function (HasMany $query) { $query->where('key', 'equipped_ilvl'); }
        ])->withCount([
            'keystones'  => function (Builder $query) use ($period) { $query->where('period', $period); },
        ])->withExists([
            'keystones as 15s_keystone_count' => function (Builder $query) use ($period) { $query->where('period', $period)->where('level', '>=', 15); },
            'keystones as 10s_keystone_count' => function (Builder $query) use ($period) { $query->where('period', $period)->where('level', '>=', 10); },
        ])->orderBy('name')->get();;
        $teams      = $user->teams()->get();

        return view(config('rk.version.ui') . '.members.index', compact('characters', 'teams'));
    }
}
