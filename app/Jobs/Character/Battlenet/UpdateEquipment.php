<?php

namespace App\Jobs\Character\Battlenet;

use App\Jobs\Unique;
use App\Models\CharacterEquipment as Equipment;
use Raidkeeper\Api\Battlenet\Client;
use Raidkeeper\Api\Battlenet\ApiResponse;

class UpdateEquipment extends Unique
{

    // Job execution handler
    public function handle(): void
    {
        $client    = new Client($this->model->region, config('rk.battlenet.id'), config('rk.battlenet.secret'));
        $character = $client->loadCharacter($this->model->name, $this->model->realm);
        $data      = $character->getEquipment();
        if ($data instanceof ApiResponse) {
            foreach ($data->equipped_items as $item) {
                $borrowedSpecial = false;
                $hasSocket       = false;

                if (isset($item->sockets)) {
                    foreach ($item->sockets as $socket) {
                        if (isset($socket->socket_type) && isset($socket->socket_type->type) && $socket->socket_type->type == "DOMINATION") {
                            $hasSocket = false;
                        } else {
                            $hasSocket = true;
                        }
                    }
                }

                if (isset($item->set)) {
                    switch($data->getLocaleData($item->set->item_set)):
                        ## Season 1 - 10.0
                        case "Stormwing Harrier's Camouflage":   # Hunter
                        case "Haunted Frostbrood Remains":       # DK
                        case "Scales of the Awakened":           # Evoker
                        case "Draconic Hierophant's Finery":     # Priest
                        case "Wrappings of the Waking Fist":     # Monk
                        case "Skybound Avenger's Flightwear":    # DH
                        case "Vault Delver's Toolkit":           # Rogue
                        case "Virtuous Silver Cataphract":       # Paladin
                        case "Stones of the Walking Mountain":   # Warrior
                        case "Scalesworn Cultist's Habit":       # Warlock
                        case "Bindings of the Crystal Scholar":  # Mage
                        case "Elements of Infused Earth":        # Shaman
                        case "Lost Landcaller's Vesture":        # Druid
                        ## Season 2 - 10.1
                        case "Lingering Phantom's Encasement":   # DK
                        case "Kinslayer's Burdens":              # DH
                        case "Strands of the Autumn Blaze":      # Druid
                        case "Legacy of Obsidian Secrets":       # Evoker
                        case "Ashen Predator's Scaleform":       # Hunter
                        case "Underlight Conjurer's Brilliance": # Mage
                        case "Fangs of the Vermillion Forge":    # Monk
                        case "Heartfire Sentinel's Authority":   # Paladin
                        case "The Furnace Seraph's Verdict":     # Priest
                        case "Lurking Specter's Shadeweave":     # Rogue
                        case "Runes of the Cinderwolf":          # Shaman
                        case "Sinister Savant's Cursethreads":   # Warlock
                        case "Irons of the Onyx Crucible":       # Warrior
                        ## Season 3 - 10.2
                        case "Risen Nightmare's Gravemantle":    # DK
                        case "Screaming Torchfiend's Brutality": # DH
                        case "Benevolent Embersage's Guidance":  # Druid
                        case "Werynkeeper's Timeless Vigil":     # Evoker
                        case "Blazing Dreamstalker's Trophies":  # Hunter
                        case "Underlight Conjurer's Brilliance": # Mage
                        case "Mystic Heron's Discipline":        # Monk
                        case "Zealous Pyreknight's Ardor":       # Paladin
                        case "Blessings of Lunar Communion":     # Priest
                        case "Lucid Shadewalker's Silence":      # Rogue
                        case "Vision of the Greatwolf Outcast":  # Shaman
                        case "Devout Ashdevil's Pactweave":      # Warlock
                        case "Molten Vanguard's Mortarplate":    # Warrior
                            $borrowedSpecial = true;
                            break;
                        default:
                            $borrowedSpecial = false;
                            break;
                    endswitch;
                }

                if (isset($item->bonus_list)) {
                    $bonuses = implode(':', $item->bonus_list);
                } else {
                    $bonuses = null;
                }

                if (! property_exists($item, 'level')) {
                    throw new \Exception(
                        sprintf(
                            "failed to correctly parse job: %s %s",
                            'character_id:'.$this->model->id,
                            'slot:'.$item->slot->type,
                        )
                    );
                } else {
                    Equipment::updateOrCreate(
                        ['character_id' => $this->model->id, 'slot' => $item->slot->type],
                        [
                            'item_id'          => $item->item->id,
                            'name'             => $data->getLocaleData($item),
                            'level'            => $item->level->value,
                            'socket'           => $hasSocket,
                            'borrowed_special' => $borrowedSpecial,
                            'enchantment'      => isset($item->enchantments) ? true : false,
                            'bonuses'          => $bonuses,
                        ],
                    );
                }
                //$model->save();
            }
        } else {
            if ($data->getCode() == '404') {
                $this->model->allow_fetch = false;
                $this->model->save();
                echo "Character not found in Battlenet API. Disabling from future updates.";
            } else {
                throw new \Exception(
                    'Unable to load character'.$this->model->name.'from Battlenet API. '.$data->getCode().': '.$data->getMessage(), 
                    $data->getCode()
                );
            }
        }
    }

    /**
     * The unique model ID of the job
     */
    public function uniqueId()
    {
        return $this->model->id;
    }
}
