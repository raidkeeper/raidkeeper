@extends('layouts.team-officer')

@section('subcontent')
<div class="row mx-2">
    <!-- Breakdown -->
    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Roster Breakdown</h3>
                    </div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-4">
                                <h2>
                                    <span class="text-{{ $breakdown['tanks'] >= 2 ? "success" : "danger"}}">
                                        {{$breakdown['tanks']}}
                                    </span>
                                </h2>
                                <h4>Tanks</h4>
                            </div>

                            <div class="col-4">
                                <h2>
                                    <span class="text-{{$breakdown['healers'] >= $breakdown['healermath'] ? 'success' : 'danger'}}">
                                        {{$breakdown['healers']}}
                                    </span>
                                </h2>
                                <h4>Healers</h4>
                            </div>
                            <div class="col-4">
                                <h2>{{$breakdown['dps']}}</h2>
                                <h4>DPS</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Applications -->
    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Pending Applications</h3>
                    </div>
                    <div class="card-body">
                        @if ($applications->count() == 0)
                            <h4 class="text-success text-center">
                                There are no pending applications.
                            </h4>
                        @else
                            <div class="table-responsive">
                                <table class="table table-sm-text-center">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Character</th>
                                            <th style="width: 10%">Role</th>
                                            <th style="width: 10%">iLvl</th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($applications->get() as $app)
                                            <tr class="text-center">
                                                <td class="cls-{{$app->character()->shortClassName()}}">
                                                    <img style="width: 20px; height: 20px;" src="/img/icons/covenants/{{$app->character()->iconCovenant()}}.png"></img>
                                                    {{$app->character()->getAttr('name')}}
                                                </td>
                                                <td><img style="width: 30px; height: 30px;" src="/img/icons/{{ $app->role }}.svg"></img></td>
                                                <td>{{$app->character()->getAttr('ilvl_equipped')}}</td>
                                                <td>
                                                    <a href="/teams/{{$team->id}}/recruitment/{{$app->id}}/accept"><i class="text-success fas fa-plus"></i></a>
                                                    <a href="/teams/{{$team->id}}/recruitment/{{$app->id}}/reject"><i class="text-danger fas fa-times"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Team Information</h3>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Team Officers</h3>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
