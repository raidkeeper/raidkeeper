@extends('v2.core.main')

@section('headline')
Welcome To Raidkeeper!
@endsection

@section('content')
<!-- Welcome Section -->
<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms mb-4 mb-lg-5">Your Overview</h2>
    <div class="row">
        <div class="col-lg-7 mb-4 mb-lg-0">
            <div class="card-widget h-100"> 
                <div class="text text-center">
                    <h1 class="mb-0">{{$characters->sum('keystones_count')}}</h1>
                    <span class="text-gray-300">completed keys this week</span>
                </div>  
                <div class="text text-center">
                    <h1 class="mb-0">{{ $characters->count() == 0 ? 0 : floor(($characters->sum('10s_keystone_count') / $characters->count()) * 100) }}%</h1>
                    <span class="text-gray-300">characters with completed +10s</span>
                </div>
                <div class="text text-center">
                    <h1 class="mb-0">{{ $characters->count() == 0 ? 0 : floor(($characters->sum('15s_keystone_count') / $characters->count()) * 100) }}%</h1>
                    <span class="text-gray-300">characters with completed +15s</span>
                </div>
            </div>
        </div>
        <div class="col-lg-5 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-header">
                    <h4 class="card-heading">Your Teams</h4>
                </div>
                <div class="card-body px-4 py-4">
                    <ul>
                        @foreach($teams as $yt)
                            <li>
                                <a href="{{$yt->url()}}">{{$yt->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Roster -->
<section class="mb-4 mb-lg-5">
    <div class="card card-table mb-4">
        <div class="card-header">
            <div class="h3 text-gray-100 text-uppercase">
                My Characters
            </div>
        </div>
        

        <table class="table table-hover mb-0" id="datatable2">
            <thead>
                <tr>
                    <th width="7%"></th>
                    <th>Character</th>
                    <th class="text-center">Item Level</th>
                    <th class="text-center">Current Great Vault</th>
                    <th class="text-center">Previous Great Vault</th>
                    <th class="text-center">M+ Score</th>
                </tr>
            </thead>
            <tbody>
                @foreach($characters as $character)
                    <tr class="rk-bg2-{{str_replace(' ', '', strtolower($character->class))}}">
                        <!-- Name -->
                        <td class="h5 rk-cls-{{str_replace(' ', '', strtolower($character->class))}}">
                            <div style="text-align: center;" class="float-left pull-left">
                                <div>
                                    @if(! $character->allow_fetch)
                                        @php $cssClass=""; $cssText="Transferred"; @endphp
                                    @else
                                        @php 
                                            $cssText=$character->realm;
                                            $faction = $character->attributes()->where('key', 'faction')->first()->value ?? "unknown";
                                            switch(strtolower($faction)):
                                                case('alliance'):
                                                    $cssClass = 'rk-bg-shaman';
                                                    break;
                                                case('horde'):
                                                    $cssClass = 'rk-bg-deathknight';
                                                    break;
                                                default:
                                                    $cssClass= 'rk-bg-warlock';
                                                    break;
                                            endswitch;
                                        @endphp
                                    @endif
                                    <span class="badge {{$cssClass}}" style="padding: 0.32em 0.65em; font-size: 11px; width: 100%; font-weight: bold; color: #f5f5f5 !important;">
                                        {{$cssText}}
                                    </span>
                                </div>
                            </div>
                        <td class="h5 rk-cls-{{str_replace(' ', '', strtolower($character->class))}}" style="vertical-align: middle;">
                            <div>
                                <span @if($character->user !== null) data-toggle="tooltip" data-placement="top" title="{{$character->user->battletag}}"@endif>
                                    <a href="/members/character/{{$character->id}}" style="color: inherit;">{{$character->name}}</a>
                                </span>
                            </div>
                        </td>
                        <!-- Item Level -->
                        <td class="text-center">{!! \App\Utils\Formatters\ItemLevel::color($character->attributes->pluck('value')->avg()) !!}</td>
                        <!-- This Weeks GV -->
                        <td class="text-center" style="font-family: monospace;">{!! \Illuminate\Support\Facades\Cache::get($character->id.'_great_vault') ?? \App\Utils\Formatters\GreatVault::parse($character->great_vault->first()) !!}</td>
                        <!-- Last Weeks GV -->
                        @if($character->great_vault->where('period', \Illuminate\Support\Facades\Cache::get('raiderio_period') - 1)->count() == 1)
                            <td class="text-center" style="font-family: monospace;">{!! \App\Utils\Formatters\GreatVault::parse($character->great_vault->where('period', \Illuminate\Support\Facades\Cache::get('raiderio_period') - 1)->first()) !!}</td>
                        @else
                            <td class="text-center" style="font-family: monospace;"><span class="rk-cls-deathknight">- - - </span>|<span class="rk-cls-deathknight"> -- -- --</span></td>
                        @endif

                        <!-- M+ Score -->
                        <td class="text-center">{!! $character->showMythicRating() !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>



    </div>
</section>
<!-- End Keystones Section -->
@endsection
