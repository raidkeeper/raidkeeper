<?php

namespace App\Policies;

use Auth;
use App\Models\Team;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Team $team
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(?User $user, Team $team)
    {
        if (filter_var($team->attributes()->where('key', 'public')->first()->value ?? false, FILTER_VALIDATE_BOOLEAN)) {
            return true;
        }

        if ($team->name == 'Slippery When Wet' && $team->region = 'us') {
            return true;
        }

        if (Auth::check()) {
            if ($team->user_id == $user->id) {
                return true;
            } elseif ($user->teams()->where('id', $team->id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Team $team)
    {
        return $user->isOfficer($team);
    }
}
