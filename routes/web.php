<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\TournamentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Authentication and OAuth2 Routes
 */
Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', [LoginController::class, 'redirectToProvider'])->name('login');
    Route::get('login/callback', [LoginController::class, 'handleProviderCallback'])->name('callback');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});

Route::get('/', [IndexController::class, 'index'])->name('home');
Route::get('/feedback', [FeedbackController::class, 'index'])->name('feedback');
Route::post('/feedback', [FeedbackController::class, 'create']);

Route::get('/tournaments/active', [TournamentController::class, 'index']);

//------------------
//   Teams Routes 
//------------------
App\Routes\Teams\GeneralRoutes::load();
App\Routes\Teams\TeamsRoutes::load();

//------------------
// Character Routes 
//------------------
App\Routes\Members\GeneralRoutes::load();