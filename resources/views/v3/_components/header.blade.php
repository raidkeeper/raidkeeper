<nav class="navbar navbar-expand navbar-dark rk-header" aria-label="Raidkeeper Title Bar">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Raidkeeper</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#upperNavbar" aria-controls="upperNavbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="upperNavbar">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link" href="#" style="font-size: 0.85rem">{{str_replace("v", "", config('rk.version.application'))}}</a>
        </li>
      </ul>
      @if(\Auth::user())
      <a href="#" class="rk-welcome">
        Welcome, {{\Auth::user()->battletag}}
      </a>
      @else
      <a href="/login" class="rk-sign-in">
        <i class="lni lni-user" style="padding-right: 5px;"></i>
        Sign In
      </a>
      @endif
    </div>
  </div>
</nav>