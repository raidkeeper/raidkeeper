@extends('errors.base')

@section('code', __('504 Gateway Timeout'))
@section('message', __('It may have been us. It may have been you. It was probably Blizzard. We\'ve been notified.'))