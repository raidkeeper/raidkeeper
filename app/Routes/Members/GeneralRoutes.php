<?php

namespace App\Routes\Members;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Members\IndexController;
use App\Http\Controllers\Members\CharactersController;
use App\Http\Controllers\Members\SettingsController;

class GeneralRoutes {

    public static function load()
    {
        //------------------
        //  Members Routes 
        //------------------   
        // We require auth here for everyone involved 
        Route::middleware('auth')->group(function () {
            Route::get('/me', [IndexController::class,    'index'])->name('user.characters');

            // We dupe the next two to allow user URL '/me' and the team officer URL '/members' 
            Route::middleware('can:view,character')->group(function() {
                Route::get('/me/character/{character}',      [CharactersController::class, 'index'])->name('user.characters.character');
                Route::get('/members/character/{character}', [CharactersController::class, 'index'])->name('team.roster.character');
            });

            Route::get('/me/settings',  [SettingsController::class, 'index'])->name('user.settings');
            Route::post('/me/settings', [SettingsController::class, 'update']);
        });

        Route::middleware('can:view,character')->group(function () {
            Route::get('/members/character/{character}', [CharactersController::class, 'index']);
        });
    }
}