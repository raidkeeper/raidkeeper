@extends('v2.core.main')

@section('headline')
<span style="font-size: 48px; text-align: center">
    @yield('code')
</span>
@endsection

@section('content')
<section class="mb-4 mb-lg-5">
  <div class="row" style="font-size: 36px; font-weight: 100;">
    @yield('message')
  </div>
</section>
@endsection