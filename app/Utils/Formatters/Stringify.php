<?php declare(strict_types=1);

namespace App\Utils\Formatters;

class Stringify
{
    public static function slugify($text): string
    {
        return str_replace(' ', '', mb_strtolower($text, 'UTF-8'));
    }
}