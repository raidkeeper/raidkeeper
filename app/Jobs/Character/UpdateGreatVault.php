<?php

namespace App\Jobs\Character;

use Illuminate\Support\Facades\Cache;
use App\Jobs\Unique;
use App\Models\GreatVault;

class UpdateGreatVault extends Unique
{
    // Job execution handler
    public function handle(): void
    {
        $period = Cache::get('raiderio_period');
        if ($period !== null) {

            $vault  = GreatVault::where(['character_id' => $this->model->id, 'period' => $period])->first();
            if ($vault == null) {
                $vault = new GreatVault;
                $vault->character_id = $this->model->id;
                $vault->period       = $period;
            }
    
            // Loading the keystones portion of the GreatVault
            $keystones = $this->model->keystones()->where('period', $period)->orderBy('level', 'DESC')->limit(10)->get()->pluck('level');
            $vault->key_1 = $keystones[config('rk.great_vault_thresholds.mplus.first') - 1]  ?? null;
            $vault->key_2 = $keystones[config('rk.great_vault_thresholds.mplus.second') - 1] ?? null;
            $vault->key_3 = $keystones[config('rk.great_vault_thresholds.mplus.third') - 1]  ?? null;
    
            // Loading the raid kills portion of the GreatVault
            $kills = $this->model->parses()->where('period', $period)->orderBy('difficulty', 'DESC')->limit(10)->get()->pluck('difficulty');
            $vault->raid_1 = $kills[config('rk.great_vault_thresholds.raid.first') - 1]  ?? null;
            $vault->raid_2 = $kills[config('rk.great_vault_thresholds.raid.second') - 1] ?? null;
            $vault->raid_3 = $kills[config('rk.great_vault_thresholds.raid.third') - 1]  ?? null;
            $vault->save();
        }
    }
}
