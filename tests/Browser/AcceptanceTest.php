<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testIndex()
    {
        // Testing authenticated front page
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('RaidKeeper')
                    ->assertDontSee('Ashkrall');
        });
    }

    public function testIndexAuthenticated(): void
    {
        // Testing authenticated front page
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
                    ->visit('/')
                    ->assertSee('RaidKeeper')
                    ->assertSee('Ashkrall');
        });
    }

    public function testTeamCreation(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
                    ->visit('/')
                    ->clickLink('Create A Raid Team')
                    ->assertSee('Lets Create A Team!');

            $browser->visit('/teams/new');

            $browser->screenshot('pre');

            $browser->type('name', 'Test Raidteam One')
                    ->type('realm', 'Sargeras')
                    ->radio('faction', 'alliance')
                    ->select('purpose', 'Raiding')
                    ->select('goal', 'Ahead of the Curve')
                    ->select('difficulty', 'Heroic')
                    ->type('timeslot', 'Wednesdays at Example time')
                    ->type('details', 'Foo bar baz fizz buzz')
                    ->click('@CreateTeamForm')
                    ->screenshot('post');
        });
    }
}
