@extends('v1.layouts.team')

@section('subcontent')
<div class="row mx-2">
    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Roster Breakdown</h3>
                    </div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-4">
                                <h2>
                                    <span class="text-{{ $breakdown['tanks'] >= 2 ? "success" : "danger"}}">
                                        {{$breakdown['tanks']}}
                                    </span>
                                </h2>
                                <h4>Tanks</h4>
                            </div>

                            <div class="col-4">
                                <h2>
                                    <span class="text-{{$breakdown['healers'] >= $breakdown['healermath'] ? 'success' : 'danger'}}">
                                        {{$breakdown['healers']}}
                                    </span>
                                </h2>
                                <h4>Healers</h4>
                            </div>
                            <div class="col-4">
                                <h2>{{$breakdown['dps']}}</h2>
                                <h4>DPS</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Applications -->
    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Average Raid Item Level</h3>
                    </div>
                    <div class="card-body">
                        <h1 class="text-center">
                            @if($ilvl >= config('rk.ilvls.mythic'))
                                <span class="text-warning">{{ $ilvl }}</span>
                            @elseif($ilvl >= config('rk.ilvls.heroic'))
                                <span class="cls-demonhunter">{{ $ilvl }}</span>
                            @elseif($ilvl >= config('rk.ilvls.normal'))
                                <span class="cls-shaman">{{ $ilvl }}</span>
                            @elseif($ilvl >= config('rk.ilvls.raid-finder'))
                                <span class="cls-hunter">{{ $ilvl }}</span>
                            @else
                                <span style="color: #999;">{{ $ilvl }}</span>
                            @endif
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breakdown -->
    <div class="col-md-6 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Bench Warmers</h3>
                    </div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-4">
                                <h2>
                                    {{$bench['tanks']}}
                                </h2>
                                <h4>Tanks</h4>
                            </div>

                            <div class="col-4">
                                <h2>
                                    {{$bench['healers']}}
                                </h2>
                                <h4>Healers</h4>
                            </div>
                            <div class="col-4">
                                <h2>{{$bench['dps']}}</h2>
                                <h4>DPS</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
