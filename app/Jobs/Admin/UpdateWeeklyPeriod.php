<?php

namespace App\Jobs\Admin;

use App\Jobs\Unique;
use Illuminate\Support\Facades\Cache;
use App\Utils\RaiderIo\MythicPlus;

class UpdateWeeklyPeriod extends Unique
{
    public function __construct() {}

    // Job execution handler
    public function handle(): void
    {
        // TODO: Change this into a multi-region supporting job
        $raiderio = new MythicPlus('', 0, '', 'us');
        $period   = $raiderio->period;
        Cache::put('raiderio_period', $period);

        $affixes   = $raiderio->getWeeklyAffixes();
        Cache::put('raiderio_affixes', $affixes->affix_details);
    }

    /**
     * The unique model ID of the job
     */
    public function uniqueId(): int
    {
        return 1;
    }
}
