<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharacterEquipment extends Model
{
    use HasFactory;

    public $fillable = [
        'character_id',
        'slot',
        'item_id',
        'name',
        'level',
        'borrowed_special',
        'socket',
        'enchantment',
        'bonuses'
    ];
}
