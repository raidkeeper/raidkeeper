<?php

namespace App\Utils\Warcraftlogs;

class Parses
{
    // User provided fields
    public string $region;
    public string $characterName;
    public string $realm;
    public int    $characterId;

    // Api fields
    public int    $zone;

    public function __construct($name, $realm, $region)
    {
        $this->region          = $this->slugify($region);
        $this->realm           = $this->slugify($realm);
        $this->characterName   = $this->slugify($name);
        $this->zone          = config('rk.warcraftlogs.raid_zone');
    }

    public function getWeeklyBossKills()
    {
        $query = http_build_query([
            'zone'    => $this->zone,
            'api_key' => config('rk.warcraftlogs.api_key')
        ]);
        $url = "https://www.warcraftlogs.com/v1/parses/character/".$this->characterName.'/'.$this->realm.'/'.$this->region."?".$query;

        $client   = new Client();
        $response = $client->getFromUrl($url);
        if ($response['error'] != null) {
            echo 'Received error response:'.$response['error'];
            return null;
        } else {
            if (is_countable($response['response'])) {
                if (count($response['response']) >= 1) {
                    return $response['response'];
                }
            } else {
                throw new \Exception(
                    "Warcraftlogs Parse Response is not countable. Content is " . serialize($response['response'])
                );
            }
            if (is_countable($response['response']) && count($response['response']) >= 1) {
                return $response['response'];
            }
        }
        return null;
    }

    protected function slugify($name): string
    {
        $name = str_replace(' ', '-', $name);
        $name = str_replace("'", '', $name);
        return strtolower($name);
    }
}
