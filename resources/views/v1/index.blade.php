@extends('v1.layouts.main')

@section('content')
    <div class="row mx-2">
        <div class="col-12 subsection">
            <div class="row mx-1 mt-2 mb-4">
                <h1 class="text-center fac-horde">RaidKeeper is in Alpha. Bugs <strong>will be common</strong>, and should be reported.</h1>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-1 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Welcome To RaidKeeper!</h3>
                        </div>
                        <div class="body px-2">
                            <p>
                                RaidKeeper is your one-stop shop for building, growing, and
                                managing a raid team.
                            </p>
                            <p>
                                Each RaidKeeper instance is configured to support a single raid team,
                                so you can use the Battle.net login button to the right to sign in
                                to this raid team's Raidkeeper.
                            </p>
                            <p>
                                Ongoing development of Raidkeeper happens on Github. If you run into bugs or
                                issues, or if you'd like to suggest enhancements or new features use the Github
                                issue tracker.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 subsection">
            <!-- Your Teams Card -->
                <div class="row mt-2 mb-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center">Log In</h3>
                            </div>
                            <div class="card-body text-center">
                                <a href="/login">
                                    <img style="border: 1px solid rgba(0, 116, 224, 0.4); border-radius: 20px; width: 256px; height: 256px;" src="/img/battlenet-logo.png" alt=""> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
