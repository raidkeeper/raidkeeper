# RaidKeeper

A straightforward raid team management platform for World of Warcraft.

[![Latest Release](https://img.shields.io/github/v/release/jgrancell/raidkeeper?style=for-the-badge)](https://github.com/jgrancell/raidkeeper/releases)

![Raidkeeper Overview](https://imgur.com/K8ic09i.png)

## What Is This?

Raidkeeper was borne as a tool to allow me to manage my own raid teams. I wanted a way 
outside of a complicated spreadsheet to monitor the vital statistics, so to speak, of
the group of raiders that I am leading. Raidkeeper solves this issue, for me at least.

Raidkeeper is a web application that provides a number of useful features for raid team
management, including:
- Tracking of active/inactive members of your raid roster.
- Tracking of item levels for both active and benched characters.
- Analysis of current active raid roster for missing buffs and present raidwide cooldowns.
- Presentation of your raiders' characters current Great Vaults (or lack thereof) for 'encouragement'.
- Attendance tracking.

## How Do I Use This?
Raidkeeper is free, open source software. You are free to install this on the web server of your choice.
If you are not technically inclined, I will be offering hosted Raidkeeper installs in the future once the
software has reached a stable state.

The only currently supported install method is through Kubernetes, as that is how the reference installation
at https://raidkeeper.com currently runs. Instructions on running in Kubernetes can be found in [the kubernetes docs](./manifests).

## The Technical Bits

#### Issues
If you run across any errors, you should submit a Github issue. Please provide clear, full stack traces for all 500 or server errors.

#### Suggestions, Enhancements, and Feature Requests
As with errors, you should submit all suggestions, requests, or enhancement ideas as Github issues. Not all requests will be accepted,
but all will be addressed.

#### Requirements
Raidkeeper is developed for and runs on:
- PHP 8 
  - 7.4 is tested and works, but is not supported.
- Postgresql 13 / Modern MariaDB.
  - MariaDB is a perfectly valid option, and should be supported by Raidkeeper. I do not test against it, however.
  - Postgresql HA is much easier in Kubernetes, so is used in the reference architecture.
  - Take backups. Regularly and often.
- Nginx
  - Other webservers will undoubtedly work, but the onus is on you to secure it properly (with .htaccess files, for example)
  - Nginx Mainline is what is used in the reference architecture.
- Redis
  - You can use other session or cache stores, such as memcached or even database, but they aren't officially supported.
  - The reference architecture uses a non-HA, non-Clustered Redis.
  - Data loss in Redis isn't important, it's only used as an API call store to save on round trips to both the database and Battle.net.

## License
This software is released under the GPLv3 license. 
```
Copyright (C) 2021  Joshua Grancell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

See the GPLv3 license in its entirety at the [LICENSE.md](./LICENSE.md) file.