<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $referer = $request->input('referer') ?? 'direct';
        return view(config('rk.version.ui') . '.feedback', compact('referer'));
    }

    public function create(Request $request)
    {
        $referer = $request->input('referring_page') ?? 'Direct';
        $type    = $request->input('type') ?? 'Question';
        $user    = $request->input('name') ?? 'Anonymous';
        $contact = $request->input('contact') ?? 'No';
        $discord = $request->input('discord') ?? 'Not Provided';
        $body    = $request->input('request');

        if ($body == null) {
            return redirect()->back();
        }

        $issueText = "**Submittor**: ".$user."\n **Type**: ".ucwords($type)."\n **Refering Page**: ".$referer."\n **Contact**: ";
        $contact === 'Yes' ? $issueText .= $discord : $issueText .= "No";
        $issueText .= "\n **Description**: \n ".$body."\n";

        // Setting up the Gitlab API request
        $url = 'https://gitlab.com/api/v4/projects/'.config('rk.gitlab.project_id').'/issues?private_token='.config('rk.gitlab.support_submission_token');
        $headers = [
            'Content-Type: application/json',
        ];

        $payload = [
            'title' => 'Support Request Pending Triage',
            'labels' => "Status::Under Review,type::".$type,
            'description' => $issueText,
        ];

        //dd($payload);

        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL            => $url,
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_HTTPHEADER    => ['Content-Type: application/json'],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POSTFIELDS     => json_encode($payload),
            ),
        );

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status == 201) {
            $alert = [
                'message' => 'Feedback submitted. Thank you!',
                'alert'   => 'success',
            ];
        } else {
            $alert = [
                'message' => 'Feedback was not submitted. Reach out on Discord for assistance.',
                'alert'   => 'danger',
            ];
        }

        return redirect()->back()->with($alert);
    }
}
