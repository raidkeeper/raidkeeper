<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_equipment', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('character_id');
            $table->string('slot');
            $table->integer('item_id');
            $table->string('name');
            $table->integer('level');
            $table->boolean('socket');
            $table->boolean('enchantment');
            $table->boolean('borrowed_special')->default(false);
            $table->string('bonuses')->nullable();
            $table->timestamps();

            $table->unique(['character_id', 'slot']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_equipment');
    }
}
