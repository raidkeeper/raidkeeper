@extends('v2.core.main')

@section('headline')
Welcome to RaidKeeper!
@endsection

@section('content')
<!-- Welcome Section -->
<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms mb-4 mb-lg-5">What is RaidKeeper?</h2>
    <div class="row">
        <div class="col-lg-7 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <p>RaidKeeper is your one-stop shop for building, growing, and managing a raid team.</p>
                    <p>Raidkeeper also supports managing other groups, such as Mythic Plus and PvP groups. Most features, however, are aimed at raid progression.</p>
                    <p>Ongoing development of Raidkeeper happens on Gitlab. If you run into bugs or issues, or if you'd like to suggest enhancements or new features use the Gitlab issue tracker.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-5 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <p>To see a limited example of some of the features that RaidKeeper provides, check out one of the public teams:</p>
                    <ul>
                        <li>
                            <a href="/teams/us/Slippery+When+Wet">Slippery When Wet</a>
                        </li>
                    </ul>
                    <p>To create your own team, use the link to the left. This will log you in with Battle.net.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms mb-4 mb-lg-5">RaidKeeper Features</h2>
    <div class="row">
        <div class="col-lg-7 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <p>Currently, RaidKeeper supports:</p>
                    <ul>
                        <li>Team Roster Management</li>
                        <li>Benched and Active characters</li>
                        <li>Weekly keystone completion tracking</li>
                        <li>Character equipment viewing, including: enchants, sockets, and domination sockets.</li>
                    </ul>
                    <p>Other upcoming planned features include:</p>
                    <ul>
                        <li>Raid night attendance.</li>
                        <li>Historical boss parse tracking.</li>
                        <li>Historical team ilvl trending.</li>
                        <li>Weekly keystone affix display.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection