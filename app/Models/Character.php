<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Character extends Model
{
    use HasFactory;

    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function attributes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CharacterAttribute::class);
    }

    public function great_vault(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(GreatVault::class);
    }

    public function keystones(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Keystone::class, 'character_keystone', 'character_id', 'keystone_id');
    }

    public function equipment(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CharacterEquipment::class);
    }

    public function parses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(RaidParse::class);
    }

    public function attendance(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CharacterAttendance::class);
    }

    // refactored
    public function teams(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Team::class, 'team_memberships', 'character_id', 'team_id')->withPivot(['role', 'benched']);
    }

    public function purge(): void
    {
        $this->great_vault()->delete();
        $this->attributes()->delete();
        $this->keystones()->delete();
        $this->equipment()->delete();
        $this->parses()->delete();
        $this->attendance()->delete();
        $this->teams()->detatch();
        $this->delete();
    }

    // Deprecated
    public function getEquipment() : mixed
    {
        $equipment = array();
        $slots = [
            'HEAD',
            'NECK',
            'SHOULDER',
            'BACK',
            'CHEST',
            'WRIST',
            'HANDS',
            'WAIST',
            'LEGS',
            'FEET',
            'FINGER_1',
            'FINGER_2',
            'TRINKET_1',
            'TRINKET_2',
            'MAIN_HAND',
            'OFF_HAND',
        ];

        foreach ($slots as $slot) {
            $equipment[$slot] = $this->equipment()->where('slot', $slot)->first();
        }
        return $equipment;
    }

    public function setAttr(string $key, string $value): void
    {
        $lookup = CharacterAttribute::where(['character_id' => $this->id, 'key' => $key]);
        if ($lookup->count() != 1) {
            $attribute = new CharacterAttribute();
            $attribute->character_id = $this->id;
            $attribute->key          = $key;
        } else {
            $attribute = $lookup->first();
        }
        $attribute->value = $value;
        $attribute->save();
    }

    public function getClass(bool $short = false)
    {
        $cacheKey = 'character_'.$this->id.'attribute_class';
        $value = Cache::get($cacheKey);
        if ($value === null || $value == '') {
            $value = $this->attributes()->where('key', 'class')->first()->value ?? '';
            if ($value === null || $value == '') {
                return '';
            }
            Cache::put($cacheKey, $value, now()->addDays(180));
        }

        return $short ? \App\Utils\Formatters\Stringify::slugify($value) : $value;
    }

    public function getClassRoles(): array
    {
        $class = $this->getClass(true);
        switch ($class) :
            case 'warrior':
            case 'deathknight':
            case 'demonhunter':
                return ['tank' => 1, 'mdps' => 3];
            case 'paladin':
            case 'monk':
                return ['tank' => 1, 'mdps' => 3, 'healer' => 2];
            case 'druid':
                return ['tank' => 1, 'mdps' => 3, 'rdps' => 4, 'healer' => 2];
            case 'warlock':
            case 'mage':
            case 'hunter':
                return ['rdps' => 4];
            case 'priest':
                return ['rdps' => 4, 'healer' => 2];
            case 'shaman':
                return ['mdps' => 3, 'rdps' => 4, 'healer' => 2];
            default:
                return ['mdps' => 3];
        endswitch;
    }

    public function hasRaidBuff(): bool
    {
        $class = $this->getClass(true) ?? 'unknown';
        return preg_match('/(warrior|priest|mage)/', strtolower($class)) ? true : false;
    }

    public function getRaidBuff(): string
    {
        $class = $this->getClass(true);
        switch (strtolower($class)) :
            case 'warrior':
                return "battle-shout";
            case 'priest':
                return "fortitude";
            case 'mage':
                return "arcane-intellect";
            default:
                return "";
        endswitch;
    }

    /**
     * @return array<string>
     **/
    public function getRaidAssist($role = null): array
    {
        $class = strtolower($this->getClass(true));
        switch ($class) :
            case 'paladin':
                return $role == 2 ? ['aura-mastery', 'devotion-aura'] : ['devotion-aura'];
            case 'warrior':
                return ['rallying-cry'];
            case 'monk':
                return $role == 2 ? ['revival', 'mystic-touch'] : ['mystic-touch'];
            case 'druid':
                return $role == 2 ? ['tranquility'] : [];
            case 'shaman':
                return $role == 2 ? ['spirit-link', 'healing-tide'] : ($role == 3 ? ['windfury'] : []);
            case 'priest':
                return $role == 2 ? [] : [];
            case 'deathknight':
                return ['anti-magic-zone'];
            case 'demonhunter':
                return ['chaos-brand'];
            default:
                return [];
        endswitch;
    }

    public function getHighestWeeklyKeystone($period = null)
    {
        $lookup = $this->keystones()->where('period', $period)->orderBy('level', 'DESC')->limit(1);
        return $lookup->first();
    }

    public function showMythicRating(): string
    {
        $rating = $this->attributes()->where('key', 'blizz_mythic_rating')->first()->value ?? "0";
        if ($rating == null || $rating == "0") {
            return '---';
        }
        $rating = intval($rating);

        if ($rating >= 2500) {       // Score breakpoint
            $class = "rk-cls-rogue";
            $ilvl = 246;
        } elseif ($rating >= 2000) { // Valor breakpoint
            $class = "rk-cls-demonhunter";
            $ilvl = 246;
        } elseif ($rating >= 1500) { // Valor breakpoint
            $class = "rk-cls-demonhunter";
            $ilvl  = 242;
        } elseif ($rating >= 1500) { // Score breakpoint
            $class = "rk-cls-demonhunter";
            $ilvl  = 239;
        } elseif ($rating >= 1000) { // Valor breakpoint
            $class = "rk-cls-shaman";
            $ilvl  = 239;
        } elseif ($rating >= 1000) {  // Score breakpoint
            $class = "rk-cls-shaman";
            $ilvl  = 236;
        } elseif ($rating >= 1000) {  // Score breakpoint
            $class = "rk-cls-shaman";
            $ilvl  = 229;
        } elseif ($rating >= 500) {  // Valor breakpoint
            $class = "rk-cls-hunter";
            $ilvl = 229;
        } elseif ($rating >= 500) {  // Score breakpoint
            $class = "rk-cls-hunter";
            $ilvl = 226;
        } else {
            $class = "";
            $ilvl = 226;
        }
        return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="Valor Item Level Max: '.$ilvl.'" class="h4 '.$class.'">'.$rating . '</span>';
    }

    public function getTierSpecialCount(): int
    {
        $items = 0;
        $equipment = CharacterEquipment::where('character_id', $this->id);
        foreach ($equipment->get() as $item) {
            $items += $item->borrowed_special ? 1 : 0;
        }
        return $items;
    }

    public function showTierSpecial(): string
    {

        $tierSpecials = $this->equipment()->where('borrowed_special', 1)->get();
        $items = [
            'HEAD'     => '<span class="text-gray-500">&#8226;</span>',
            'SHOULDER' => '<span class="text-gray-500">&#8226;</span>',
            'CHEST'    => '<span class="text-gray-500">&#8226;</span>',
            'LEGS'     => '<span class="text-gray-500">&#8226;</span>',
            'HANDS'    => '<span class="text-gray-500">&#8226;</span>',
        ];

        foreach($tierSpecials as $gear) {
            if ($gear->level >= config('rk.ilvls.mythic')) {
                $color = 'h4 rk-cls-rogue';
            } elseif($gear->level >= config('rk.ilvls.heroic')) {
                $color = 'h4 rk-cls-demonhunter';
            } elseif ($gear->level >= config('rk.ilvls.normal')) {
                $color = 'h4 rk-cls-shaman';
            } else {
                $color = 'h4 rk-cls-hunter';
            }

            $items[$gear->slot] = '<a href="#" class="'.$color.'" data-wowhead="item='.$gear->item_id.'&ilvl='.$gear->level.'&bonus='.$gear->bonuses.'">';
            $items[$gear->slot] .= $gear->slot == 'HANDS' ? 'G' : substr($gear->slot, 0, 1);
            $items[$gear->slot] .= '</a>';
        }

        return implode(' <span class="text-gray-500">|</span> ', $items);
    }

    public function getAttendance($id): int
    {
        $lookup = $this->attendance()->where('team_attendance_id', $id);
        if ($lookup->count() == 1) {
            return $lookup->first()->status;
        }
        return 0;
    }

    public function getStandardizedClass(): string
    {
        return strtolower(str_replace(' ', '-', $this->class));
    }
}
