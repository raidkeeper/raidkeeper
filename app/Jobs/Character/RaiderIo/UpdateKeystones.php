<?php

namespace App\Jobs\Character\RaiderIo;

use App\Jobs\Unique;
use App\Models\Keystone;
use App\Models\GreatVault;
use App\Utils\RaiderIo\MythicPlus;

class UpdateKeystones extends Unique
{
    // Job execution handler
    public function handle(): void
    {
        $prefix = uniqid($this->model->id.'-rio-');
        $raiderio = new MythicPlus(
            $this->model->name,
            $this->model->id,
            $this->model->realm,
            $this->model->region
        );

        $data = $raiderio->getWeeklyKeystones();
        if ($data !== null) {
            foreach ($data['weekly_keys'] as $key) {
                $season = explode('/', $key->url)[4];
                $id     = explode('-', explode('/', $key->url)[5])[0];
                $raiderioID  = sprintf("%s-%s", $season, $id);
                $lookup = Keystone::where(['raiderio_id' => $raiderioID]);
                if ($lookup->count() == 0) {
                    $keystone = new Keystone();
                    $keystone->period       = $raiderio->period;
                    $keystone->character_id = $this->model->id;
                    $keystone->level        = $key->mythic_level;
                    $keystone->completed_at = $key->completed_at;
                    $keystone->dungeon      = $key->dungeon;
                    $keystone->raiderio_url = $key->url;
                    $keystone->raiderio_id  = $raiderioID;
                    $keystone->num_keystone_upgrades = $key->num_keystone_upgrades;
                    $keystone->score = $key->score;
                    $keystone->clear_time_ms = $key->clear_time_ms;
                    $keystone->save();
                } else {
                    $keystone = $lookup->first();
                    $keystone->num_keystone_upgrades = $key->num_keystone_upgrades;
                    $keystone->score = $key->score;
                    $keystone->clear_time_ms = $key->clear_time_ms;
                    $keystone->save();
                }
                if ($this->model->keystones()->where('keystone_id', $keystone->id)->count() == 0) {
                    $keystone->characters()->attach($this->model->id);
                }
            }

            $this->model->setAttr('bliz_mythic_rating', $data['score']);
        } else {
            echo $prefix.'    X Character has no keystones.'.PHP_EOL;
        }
    }
}
