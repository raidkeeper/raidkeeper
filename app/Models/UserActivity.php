<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    use HasFactory;

    public static function log(int $user_id, string $type, string $notes) : void
    {
        $activity          = new self();
        $activity->user_id = $user_id;
        $activity->type    = $type;
        $activity->notes   = $notes;
        $activity->save();
    }
}
