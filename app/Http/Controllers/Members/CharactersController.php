<?php

namespace App\Http\Controllers\Members;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Character;
use App\Utils\RaiderIo\MythicPlus;

class CharactersController extends Controller
{
    public function index(Character $character, Request $request): \Illuminate\View\View
    {
        $team = $character->teams()->where('team_id', $request->input('team'))->first();
        $items = $character->equipment();

        $equipment = $character->getEquipment();

        $raiderio = new MythicPlus('', 0, '', 'us');
        $period   = $raiderio->period;

        return view(config('rk.version.ui') . '.members.character', compact('team', 'character', 'equipment', 'period'));
    }
}
