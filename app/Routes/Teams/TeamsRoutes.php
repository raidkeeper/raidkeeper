<?php

namespace App\Routes\Teams;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Teams\AttendanceController;
use App\Http\Controllers\Teams\DashboardController;
use App\Http\Controllers\Teams\MetricsController;
use App\Http\Controllers\Teams\RosterController;
use App\Http\Controllers\Teams\SettingsController;

class TeamsRoutes {

    public static function load()
    {
        /**
         * These are the RaidKeeper team-specific routes. These routes include:
         *   - Team dashboard / overview
         *   - Team attendance
         *   - Team roster
         *   - Team member drilldown
         */
        Route::prefix('/teams/{region}/{team}')->middleware('can:view,team')->group(function () {
            /**
             * This route is protected by the "view" middleware, meaning it can
             * only be accessed if:
             *   - The team is public
             *   - The user is logged in and a member of the team
             */
            Route::get('/', [DashboardController::class, 'index'])->name('team.dashboard');
            Route::get('/roster/{filter?}', [RosterController::class, 'index'])->where(
                'filter',
                '(active|bench)',
            )->name('team.roster');

            Route::middleware(['auth', 'can:update,team'])->group(function () {
                /**
                 * These routes require Officer or Owner access to a team 
                 */    
                Route::get('/metrics/keystones', [MetricsController::class, 'keystones'])->name('team.metrics.keystone');
                
                Route::get('/settings',  [SettingsController::class, 'index'])->name('team.settings');
                Route::post('/settings', [SettingsController::class, 'create']);
                
                Route::post('/settings/officers/promote', [SettingsController::class, 'promote']);
                Route::post('/settings/officers/demote',  [SettingsController::class, 'demote']);

                Route::post('/roster',                     [RosterController::class, 'update']);
                Route::post('/roster/refresh/{character}', [RosterController::class, 'refresh']);
                Route::delete('/roster',                   [RosterController::class, 'remove']);

                Route::get('/attendance/',     [AttendanceController::class, 'index'])->name('team.attendance');
                Route::get('/attendance/new',  [AttendanceController::class, 'create'])->name('team.attendance.create');
                Route::post('/attendance/new', [AttendanceController::class, 'save']);
            });

            /**
             * This route is protected by the "view" middleware, meaning it can
             * only be accessed if:
             *   - The team is public
             *   - The user is logged in and a member of the team
             */
        });
    }
}