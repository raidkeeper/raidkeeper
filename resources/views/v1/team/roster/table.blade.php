    <!-- Active Roster -->
    <div class="col-md-12 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">@if($bench) Benched Characters @else Team Roster @endif</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="active-roster" class="table table-sm text-center">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Character</th>
                                        <th style="width: 20%">Member</th>
                                        <th style="width: 15%">Raidwides</th>
                                        <th style="width: 10%">iLvl</th>
                                        <th style="width: 30%; text-align: center;">
                                            <form action="/teams/{{$team->id}}/roster" method="GET">
                                                <select class="form-control" onchange="this.form.submit()" style="font-size: 1.2rem; text-align: center;" name="mode" id="mode">
                                                    <option value="great_vault" @if($mode == 'great_vault') {{'selected'}} @endif>
                                                        Great Vault
                                                    </option>
                                                    <option value="mythic_score" @if($mode == 'mythic_score') {{'selected'}} @endif>
                                                        Mythic Score
                                                    </option>
                                                    <option value="domination_sockets" @if($mode == 'domination_sockets') {{'selected'}} @endif>
                                                        Dom Sockets
                                                    </option>
                                                    <option value="covenant" @if($mode == 'covenant') {{'selected'}} @endif>
                                                        Covenant
                                                    </option>
                                                </select>
                                            </form>
                                        </th>
                                        @if($user->isOfficer($team))
                                            <th style="width: 5%"></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($characters as $character)
                                        @if ($character->isBenched($team) == $bench)
                                            <tr class="bk-{{$character->shortClassName()}}">
                                                <td class="roster-row cls-{{$character->shortClassName()}}" style="text-align: left;">
                                                    <img style="width: 32px; height: 32px;" src="/img/icons/{{$character->getRole($team)}}.svg" alt="{{$character->getRole($team)}} Role Icon">
                                                    @if($user->isOfficer($team) || $user->id == $character->user_id)
                                                        <a href="/teams/{{$team->id}}/character/{{$character->id}}">
                                                            {{ $character->name }}
                                                        </a>
                                                    @else
                                                        <h6 style="font-size: 60%; margin: 0px;">{{ 'Hidden' }}</h6>
                                                    @endif
                                                </td>

                                                <td class="roster-row">
                                                    <span class="badge bg-secondary">Unregistered</span>
                                                </td>

                                                <td class="roster-row">
                                                    @if($character->hasRaidBuff())
                                                        <img class="roster-icon raid-buff" src="/img/icons/spells/{{$character->getRaidBuff()}}.jpg"></img>
                                                    @endif
                                                    @foreach($character->getRaidCooldown() as $cooldown)
                                                        <img class="roster-icon raid-cd" src="/img/icons/spells/{{$cooldown}}.jpg"></img>
                                                    @endforeach
                                                </td>

                                                <td class="roster-row">
                                                    {!! \App\Utils\Formatters\ItemLevel::color($character->getAttr('equipped_ilvl')) !!}
                                                </td>


                                                <td class="roster-row mono-font">
                                                    @if ($mode == 'domination_sockets')
                                                        {!! $character->getDominationSockets() !!}
                                                    @elseif ($mode == 'mythic_score')
                                                        {!! $character->showMythicRating() !!}
                                                    @elseif ($mode == 'covenant')
                                                        <img class="roster-icon" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png"></img>
                                                    @else
                                                        {!! $character->showGreatVault($period) !!}
                                                    @endif
                                                </td>

                                                @if($user->isOfficer($team))
                                                    <td class="roster-row">
                                                        <div class="dropdown">
                                                            <a style="color: rgba(0, 116, 224, 0.9);" data-bs-toggle="dropdown" aria-expanded="false" id="button-{{$character->id}}"><i class="fas fa-ellipsis-h"></i></a>
                                                            <ul class="dropdown-menu" aria-labelledby="button-{{$character->id}}">
                                                                @foreach($character->getClassRoles() as $role)
                                                                    @if($role != $character->getRole($team))
                                                                        <li><a class="dropdown-item" href="#" onclick="roleSwap{{$character->id.$role}}()">Swap To {{ucwords($role)}}</a></li>
                                                                        <form action="/teams/{{$team->id}}/roster/role/{{$character->id}}" method="POST" id="roleSwap{{$character->id.$role}}">
                                                                            @csrf
                                                                            <input type="hidden" name="character_id" value="{{$character->id}}">
                                                                            <input type="hidden" name="role_name" value="{{$role}}">
                                                                        </form>
                                                                        <script>
                                                                            function roleSwap{{$character->id.$role}}() {
                                                                                document.getElementById("roleSwap{{$character->id.$role}}").submit();
                                                                            }
                                                                        </script>
                                                                    @endif
                                                                @endforeach
                                                                <li class="dropdown-divider"></li>
                                                                @if($bench)
                                                                    <li><a class="dropdown-item" href="#" onclick="activate{{$character->id}}()">Activate Character</a></li>
                                                                @else
                                                                    <li><a class="dropdown-item" href="#" onclick="bench{{$character->id}}()">Bench Character</a></li>
                                                                @endif
                                                                <li><a class="dropdown-item" href="#" onclick="delete{{$character->id}}()">Delete Character</a></li>
                                                            </ul>
                                                        </div>
                                                        <form action="/teams/{{$team->id}}/roster/activate/{{$character->id}}" method="POST" id="bench{{$character->id}}">
                                                            @csrf
                                                            <input type="hidden" name="action" value="bench">
                                                        </form>
                                                        <form action="/teams/{{$team->id}}/roster/activate/{{$character->id}}" method="POST" id="activate{{$character->id}}">
                                                            @csrf
                                                            <input type="hidden" name="action" value="activate">
                                                        </form>
                                                        <form action="/teams/{{$team->id}}/roster" method="POST" id="delete{{$character->id}}">
                                                            @csrf
                                                            {{ method_field('DELETE') }}
                                                            <input type="hidden" name="character_id" value="{{$character->id}}">
                                                        </form>
                                                        <script>
                                                            function activate{{$character->id}}() {
                                                                document.getElementById("activate{{$character->id}}").submit();
                                                            }                                                            
                                                            function bench{{$character->id}}() {
                                                                document.getElementById("bench{{$character->id}}").submit();
                                                            }
                                                            function delete{{$character->id}}() {
                                                                document.getElementById("delete{{$character->id}}").submit();
                                                            }
                                                        </script>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>