@extends('v1.layouts.team')

@section('subcontent')
<div class="row mx-2">
    <!-- Active Roster -->
    <div class="col-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h3 class="text-center">Submit New Raid Attendance</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form method="POST" action="/teams/{{$team->id}}/attendance/new">
                                <table id="active-roster" class="table table-sm text-center">
                                    @csrf
                                    <input type="hidden" name="team_id" value="1">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">Role</th>
                                            <th>Character</th>
                                            <th style="width: 40%">Attendance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($characters as $character)
                                            @if (! $character->isBenched($team)
                                                <tr>
                                                    <td>
                                                        <img style="width: 32px; height: 32px;" src="/img/icons/{{$character->getRole($team)}}.svg" alt="{{$character->getRole()}} Role Icon">
                                                    </td>
                                                    <td class="cls-{{$character->shortClassName()}}" style="vertical-align: middle;">
                                                        @if($user->isOfficer($team) || $user->id == $character->user_id)
                                                            <a href="/teams/{{$team->id}}/character/{{$character->id}}">
                                                                {{ $character->name }}
                                                            </a>
                                                        @else
                                                            <h6 style="font-size: 60%; margin: 0px;">{{ 'Hidden' }}</h6>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Attendance check for {{$character->name}}">
                                                            <input type="radio" class="btn-check" name="attendance[{{$character->id}}]" id="{{$character->id}}-present" value="1" autocomplete="off">
                                                            <label class="btn btn-outline-success" for="{{$character->id}}-present">Present</label>

                                                            <input type="radio" class="btn-check" name="attendance[{{$character->id}}]" id="{{$character->id}}-excused" value="2" autocomplete="off">
                                                            <label class="btn btn-outline-secondary" for="{{$character->id}}-excused">Excused</label>

                                                            <input type="radio" class="btn-check" name="attendance[{{$character->id}}]" id="{{$character->id}}-absent" value="3" autocomplete="off">
                                                            <label class="btn btn-outline-danger" for="{{$character->id}}-absent">Absent</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input required class="form-control cls-shaman" style="width: 50%; margin: auto; border-color: #f5f5f5;" type="date" name="date">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input type="submit" name="Submit" class="btn btn-primary">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
