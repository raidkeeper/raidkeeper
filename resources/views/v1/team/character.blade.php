@extends('v1.layouts.team')

@section('subcontent')
<script>const whTooltips = {colorLinks: true, iconizeLinks: true, renameLinks: true};</script>
<script src="https://wow.zamimg.com/widgets/power.js"></script>
<div class="row mx-2">
    <div class="col-md-4 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center cls-{{$character->shortClassName()}}">{{$character->name}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <img style="width: 52px; height: 52px;" src="/img/icons/classes/{{$character->shortClassName()}}.png" >
                                <img style="width: 52px; height: 52px;" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png" alt="">
                                <img style="width: 52px;" src="/img/icons/{{$character->getRole($team)}}.svg" >
                                <img style="width: 26px;" src="/img/icons/{{$character->getRole($team)}}.svg" >
                            </div>
                            <div class="col-7">
                                <ul style="list-style: none;">
                                    <li>
                                        GV: {!! $character->showGreatVault($period) !!}
                                    </li>
                                    <li>
                                        {{$character->getAttr('renown')}} Renown
                                    </li>
                                    <li>
                                        {{$character->getAttr('equipped_ilvl')}} ilvl
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breakdown -->
    <div class="col-md-8 col-sm-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Gear and Shit</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive">
                            <thead>
                                <th>Item</th>
                                <th>Level</th>
                                <th>Modifiers</th>
                            </thead>
                            <tbody>
                                @foreach($equipment as $item)
                                    @if($item != null)
                                    <tr>
                                        <td><a href="#" data-wowhead="item={{$item->item_id}}&ilvl={{$item->level}}">Test</a></td>
                                        <td>
                                            @if($item->level >= config('rk.ilvls.mythic'))
                                            <span class="text-warning">{{ $item->level }}</span>
                                            @elseif($item->level >= config('rk.ilvls.heroic'))
                                                <span class="cls-demonhunter">{{ $item->level }}</span>
                                            @elseif($item->level >= config('rk.ilvls.normal'))
                                                <span class="cls-shaman">{{ $item->level }}</span>
                                            @elseif($item->level >= config('rk.ilvls.raid-finder'))
                                                <span class="cls-hunter">{{ $item->level }}</span>
                                            @else
                                                <span style="color: #999;">{{ $item->level }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->borrowed_special)
                                                <span class="cls-deathknight">Domination</span>
                                            @endif
                                            @if($item->enchantment)
                                                <span class="cls-shaman">Enchanted</span>
                                            @endif
                                            @if($item->socket)
                                                <span class="cls-monk">Socketed</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
