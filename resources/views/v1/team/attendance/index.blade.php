@extends('v1.layouts.team')

@section('subcontent')
<div class="row mx-2">
    <!-- Active Roster -->
    <div class="col-12 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h3 class="text-center">Team Attendance Tracking</h3>
                        <a class="cls-shaman" href="/teams/{{$team->id}}/attendance/new">Take Attendance</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="active-roster" class="table table-sm text-center">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Character</th>
                                        @foreach($periods as $period)
                                            <th>{{$period->date}}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($characters as $character)
                                        @if (! $character->isBenched($team))
                                            <tr class="bk-{{$character->shortClassName()}}">
                                                <td class="cls-{{$character->shortClassName()}}" style="vertical-align: middle; text-align: left;">
                                                    <img style="width: 32px; height: 32px;" src="/img/icons/{{$character->getRole($team)}}.svg" alt="{{$character->getRole($team)}} Role Icon">
                                                    @if($user->isOfficer($team) || $user->id == $character->user_id)
                                                        <a href="/teams/{{$team->id}}/character/{{$character->id}}">
                                                            {{ $character->name }}
                                                        </a>
                                                    @else
                                                        <h6 style="font-size: 60%; margin: 0px;">{{ 'Hidden' }}</h6>
                                                    @endif
                                                </td>
                                                @foreach($periods as $period)
                                                    <td>
                                                        @if($character->getAttendance($period->id) == 1)
                                                            <span class="badge bg-success">Present</span>
                                                        @elseif($character->getAttendance($period->id) == 2)
                                                            <span class="badge bg-secondary">Excused</span>
                                                        @elseif($character->getAttendance($period->id) == 3)
                                                            <span class="badge bg-danger">Absent</span>
                                                        @endif
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('active-roster').DataTable()
                            } );
                        </script>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
