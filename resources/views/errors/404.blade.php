@extends('errors.base')

@section('code', __('404 Page Not Found'))
@section('message', __('The page your looking for is missing. Try again, or form a search party.'))